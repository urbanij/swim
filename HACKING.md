## Testing

Run with `cargo test`.

Since Swim is a build tool for Spade, most tests require a Spade compiler. Since
this is a very expensive operation it is disabled by default. The compiler can
take a while to compile, so you can point the test suite to a local copy of the
Spade repository which will be used instead of downloading and building from
scratch.

To use a local copy, use the environment variable `SWIM_LOCAL_SPADE=<path>`, as in

```
SWIM_LOCAL_SPADE=../spade cargo test
```

You can use both relative and absolute paths to refer to the compiler.

To let Swim download and build the compiler, instead set `SWIM_DOWNLOAD_SPADE`:

```
SWIM_DOWNLOAD_SPADE=1 cargo test
```
