# Changelog

All notable changes to this project will be documented in this file.

Spade is currently unstable and all 0.x releases are expected to contain
breaking changes. Releases are mainly symbolic and are done on a six-week
release cycle. Every six weeks, the current master branch is tagged and
released as a new version.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.4.0]

### Added

- [!94][!94] Tests are now run in parallel, and all test functions are run in individual cocotb contexts. Use the `-j` flag to specify how many parallel tasks to run.
- [!94][!94] A new environment variable `SWIM_ROOT` containing the absolute path to the swim project is set for tests
- [!98][!98] Add clickable links to open the VCD file from each test in gtkwave or surfer. Support for this must be set up by running `swim setup-links`

### Fixed

### Changed

- [!95][!95] *Breaking change* Add a library name field to `swim.toml`
- [!95][!95] Ensure that dependency names match the name specified in the dependency

### Removed

### Internal


[!94]: https://gitlab.com/spade-lang/swim/-/merge_requests/94
[!95]: https://gitlab.com/spade-lang/swim/-/merge_requests/95

## [0.3.0]

### Added

- [!89][!89]: Add the ability for plugins to define custom commands
- [!92][!92]: Write a list of libraries to `${BUILD_DIR}/libraries.json`

### Fixed

- [!84][!84]: The command `swim update-spade` has been fixed so it actually works.
- [!87][!87]: Stop rebuliding spade-python twice if changes occurred

### Changed

### Removed

### Internal

[!84]: https://gitlab.com/spade-lang/swim/-/merge_requests/84
[!87]: https://gitlab.com/spade-lang/swim/-/merge_requests/87
[!89]: https://gitlab.com/spade-lang/swim/-/merge_requests/89
[!92]: https://gitlab.com/spade-lang/swim/-/merge_requests/92
[!94]: https://gitlab.com/spade-lang/swim/-/merge_requests/94
[!95]: https://gitlab.com/spade-lang/swim/-/merge_requests/95
[!98]: https://gitlab.com/spade-lang/swim/-/merge_requests/98

## [0.2.0] - 2023-04-20

- [!82][!82]*Breaking change*: Stop including stdlib and prelude in spade builds.

[!82]: https://gitlab.com/spade-lang/swim/-/merge_requests/82

## [0.1.0] - 2023-03-07

Initial numbered version

[Unreleased]: https://gitlab.com/spade-lang/swim/-/compare/v0.4.0...master
[0.4.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/spade-lang/swim/-/tree/v0.1.0
