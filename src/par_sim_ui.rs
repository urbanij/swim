use ansi_escapes::{CursorLeft, CursorPrevLine, EraseDown};
use colored::Colorize;
use console::{measure_text_width, Term};
use futures::{future::join_all, Future};
use is_terminal::IsTerminal;
use itertools::Itertools;
use log::info;
use std::{
    io::{self, Write},
    sync::Arc,
};
use tokio::sync::{
    oneshot::{self, Receiver},
    Mutex,
};
use tokio_stream::StreamExt;

use color_eyre::Result;

#[derive(Clone, PartialOrd, PartialEq, Ord, Eq)]
#[must_use]
pub enum CommandStatus {
    Failure,
    Success,
    Running,
    Waiting,
}

#[derive(Clone)]
pub struct TestState {
    pub name: String,
    pub status: Arc<Mutex<CommandStatus>>,
    pub stdio: Arc<Mutex<String>>,
}

#[derive(Clone)]
struct TestStateLocal {
    pub name: String,
    pub status: CommandStatus,
    pub stdio: String,
}

pub struct UiConfig {
    stdout_lines: usize,
    description: &'static str,
    list_ok_text: &'static str,
    text_ok_text: &'static str,
}
impl UiConfig {
    pub fn test() -> Self {
        Self {
            stdout_lines: 2,
            description: "Running tests: ",
            list_ok_text: "PASS",
            text_ok_text: "PASSED",
        }
    }

    pub fn command(description: &'static str) -> Self {
        Self {
            stdout_lines: 2,
            description,
            list_ok_text: "done",
            text_ok_text: "done",
        }
    }
}

pub async fn run_par_thread_ui(
    tests: Vec<TestState>,
    exit_rx: Receiver<()>,
    config: &UiConfig,
) -> Result<()> {
    if std::io::stdout().is_terminal() {
        run_interactive_ui(&mut Term::stdout(), &tests, exit_rx, config).await?;
    } else {
        run_text_ui(&tests, exit_rx, config).await?;
    }

    Ok(())
}

fn text_status(_term_size: (u16, u16), tests: &[TestStateLocal], config: &UiConfig) -> Vec<String> {
    let waiting = tests
        .iter()
        .filter(|t| t.status == CommandStatus::Waiting)
        .count();
    let running = tests
        .iter()
        .filter(|t| t.status == CommandStatus::Running)
        .count();
    let success = tests
        .iter()
        .filter(|t| t.status == CommandStatus::Success)
        .count();
    let failure = tests
        .iter()
        .filter(|t| t.status == CommandStatus::Failure)
        .count();

    vec![format!(
        "  {}: {} {} {} {}",
        config.description,
        format!("{waiting} waiting").dimmed(),
        format!("{running} running").purple(),
        format!("{success} {}", config.text_ok_text).green(),
        format!("{failure} FAILED").red()
    )]
}

/// Prints the status of all tests with one line per test, and 2 lines
/// of stdout per test. Stdout is truncated to term_width
fn test_list_full(
    (_term_height, term_width): (u16, u16),
    tests: &[TestStateLocal],
    config: &UiConfig,
) -> Vec<String> {
    tests
        .iter()
        .flat_map(|test| {
            let (msg, extra) = match test.status {
                CommandStatus::Waiting => ("Waiting".dimmed(), None),
                CommandStatus::Running => {
                    let status = "Running".magenta();
                    let msgs = test
                        .stdio
                        .lines()
                        .rev()
                        .chain(vec!["", ""])
                        .take(config.stdout_lines)
                        .map(|l| {
                            l.chars()
                                .take((term_width - 1) as usize)
                                .collect::<String>()
                        })
                        .collect::<Vec<_>>()
                        .into_iter()
                        .rev()
                        .join("\n");
                    (status, Some(msgs.normal()))
                }
                CommandStatus::Success => (config.list_ok_text.green(), None),
                CommandStatus::Failure => (format!("FAIL").bright_red(), None),
            };

            format!(
                " {msg} {name}{extra}\n",
                name = test.name,
                extra = extra
                    .map(|extra| format!("\n{extra}").normal())
                    .unwrap_or_else(|| "".normal())
            )
            .lines()
            .map(str::to_string)
            .flat_map(|s| manually_break_lines(&s, term_width as usize))
            .collect::<Vec<_>>()
        })
        .collect()
}

/// Prints the a running status and the stdout of all tests that are running, but
/// does compresses the non-running tests into a single line
fn test_list_running(
    term_size: (u16, u16),
    tests: &[TestStateLocal],
    config: &UiConfig,
) -> Vec<String> {
    let mut running_status = test_list_full(
        term_size,
        &tests
            .iter()
            .filter(|t| t.status == CommandStatus::Running)
            .cloned()
            .collect::<Vec<_>>(),
        config,
    );

    running_status.append(&mut text_status(term_size, &tests, config));

    running_status
}

fn test_list_dots(
    term_size @ (_term_height, term_width): (u16, u16),
    tests: &[TestStateLocal],
    spinner: &str,
    config: &UiConfig,
) -> Vec<String> {
    let all_dots = tests.iter().map(|t| match t.status {
        CommandStatus::Waiting => ".".dimmed(),
        CommandStatus::Running => spinner.magenta(),
        CommandStatus::Success => "".green(),
        CommandStatus::Failure => "✗".bright_red().bold(),
    });

    let padding = 4;
    let lines = all_dots
        .chunks((term_width - padding) as usize)
        .into_iter()
        .map(|mut line| format!("{}{}", " ".repeat(padding as usize), line.join("")))
        .collect::<Vec<_>>();

    text_status(term_size, tests, config)
        .into_iter()
        .chain(lines.into_iter())
        .collect()
}

pub fn format_failure_report(name: &str, stdio: &str, border_width: usize) -> String {
    let border = "=".repeat(border_width);
    format!(
        "{border}\n {fail_marker}: {name}\n{border}\n{stdio}\n",
        fail_marker = "FAILED".red()
    )
}

fn print_stdout(term_width: usize, state: &TestStateLocal, printed: &mut bool) {
    if matches!(state.status, CommandStatus::Failure) && !*printed {
        *printed = true;
        let to_print = format_failure_report(&state.name, &state.stdio, term_width as usize);

        for line in to_print.lines() {
            // We need to erase anything we previously drew on these lines
            // The easiest way to do that is to just cover them with spaces
            // Because lines can span more than one visual line, we have to
            // split it into chunks manually.
            let line = manually_break_lines(line, term_width as usize)
                .into_iter()
                .map(|l| {
                    let len = measure_text_width(&l);
                    l + &" ".repeat(term_width as usize - len)
                })
                .join("\n");
            println!("{}", line);
        }
    }
}

async fn run_interactive_ui(
    term: &mut Term,
    tests: &Vec<TestState>,
    mut exit_rx: Receiver<()>,
    config: &UiConfig,
) -> io::Result<()> {
    let spinners = ["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"];
    let mut spinner = 0;
    let mut stdio_printed = tests.iter().map(|_| false).collect::<Vec<_>>();
    loop {
        spinner += 1;
        if spinner >= spinners.len() * 10 {
            spinner = 0
        }

        let term_size @ (term_height, term_width) = term.size();

        // NOTE: This clone shouldn't be required, but removing it triggers
        // a compiler bug
        // lifeguard rust#https://github.com/rust-lang/rust/issues/102211
        let mut test_states = tokio_stream::iter(tests.clone())
            .then(|t| async move {
                TestStateLocal {
                    name: t.name,
                    status: t.status.lock().await.clone(),
                    stdio: t.stdio.lock().await.clone(),
                }
            })
            .collect::<Vec<_>>()
            .await;

        test_states.sort_by_key(|t| t.name.clone());

        // Print the stdout if this test just failed
        for (status, printed) in test_states.iter().zip(stdio_printed.iter_mut()) {
            print_stdout(term_width as usize, status, printed)
        }

        let status_lines = vec![
            test_list_full(term_size, &test_states, config),
            test_list_running(term_size, &test_states, config),
            test_list_dots(term_size, &test_states, spinners[spinner / 10], config),
            text_status(term_size, &test_states, config),
        ]
        .into_iter()
        .find(|lines| lines.len() <= term_height as usize)
        .unwrap_or_else(|| vec![spinners[spinner / 10].to_string()]);

        redraw(true, &status_lines.join("\n"), true);

        let exit = tokio::select! {
            _ = tokio::time::sleep(tokio::time::Duration::from_millis(10)) => false,
            _ = &mut exit_rx => {
                true
            }
        };

        if exit
            || test_states.iter().all(|state| {
                matches!(
                    state.status,
                    CommandStatus::Success | CommandStatus::Failure { .. }
                )
            })
        {
            for (status, printed) in test_states.iter().zip(stdio_printed.iter_mut()) {
                print_stdout(term_width as usize, status, printed)
            }
            // Draw the same thing again, so we don't have old content under whatever we're drawing
            // next
            redraw(true, &status_lines.join("\n"), false);
            break Ok(());
        }
    }
}

fn redraw(ansi: bool, contents: &str, redraw: bool) {
    let stderr = std::io::stderr();
    let mut stderr = stderr.lock();
    if ansi {
        let line_count = contents.chars().filter(|c| *c == '\n').count();
        write!(&mut stderr, "{}{}{}", EraseDown, contents, CursorLeft).unwrap();
        if redraw {
            for _ in 0..line_count {
                write!(&mut stderr, "{}", CursorPrevLine).unwrap();
            }
        }
    } else {
        writeln!(&mut stderr, "{}", contents).unwrap();
    }
}

#[derive(PartialEq, Eq)]
enum TextStatus {
    Waiting,
    PrintedRunning,
    PrintedOutput,
}

async fn run_text_ui(
    tests: &Vec<TestState>,
    mut exit_rx: Receiver<()>,
    config: &UiConfig,
) -> io::Result<()> {
    let mut text_statuses = tests
        .iter()
        .map(|_| TextStatus::Waiting)
        .collect::<Vec<_>>();
    loop {
        tokio::select! {
            _ = tokio::time::sleep(tokio::time::Duration::from_millis(10)) => {},
            _ = &mut exit_rx => {
                break Ok(());
            }
        }

        let mut test_states = join_all(tests.iter().map(|t| async {
            (
                t.name.clone(),
                t.status.lock().await.clone(),
                t.stdio.lock().await.clone(),
            )
        }))
        .await;

        test_states.sort();

        for ((cmd, cmd_status, stdio), text_status) in
            test_states.iter().zip(text_statuses.iter_mut())
        {
            match (cmd_status, &text_status) {
                (CommandStatus::Waiting, TextStatus::Waiting) => {}
                (CommandStatus::Running, TextStatus::Waiting) => {
                    info!("Running {cmd}");
                    *text_status = TextStatus::PrintedRunning;
                }
                (
                    CommandStatus::Failure | CommandStatus::Success,
                    TextStatus::Waiting | TextStatus::PrintedRunning,
                ) => {
                    if text_status == &TextStatus::Waiting {
                        info!("Running {cmd}");
                    }
                    if cmd_status == &CommandStatus::Failure {
                        println!("{}", format_failure_report(cmd, stdio, 80));
                        info!("{cmd}: {}", "FAILED".red())
                    } else {
                        info!("{cmd}: {}", config.text_ok_text.green())
                    }
                    *text_status = TextStatus::PrintedOutput;
                }
                (_, TextStatus::PrintedRunning) => {}
                (_, TextStatus::PrintedOutput) => {}
            }
        }

        // NOTE: This clone shouldn't be required, but removing it triggers
        // a compiler bug
        // lifeguard rust#https://github.com/rust-lang/rust/issues/102211
        let test_states = tokio_stream::iter(tests.clone())
            .then(|t| async move {
                TestStateLocal {
                    name: t.name,
                    status: t.status.lock().await.clone(),
                    stdio: t.stdio.lock().await.clone(),
                }
            })
            .collect::<Vec<_>>()
            .await;

        if test_states.iter().all(|state| {
            matches!(
                state.status,
                CommandStatus::Success | CommandStatus::Failure { .. }
            )
        }) {
            // Draw the same thing again, so we don't have old content under whatever we're drawing
            // next
            break Ok(());
        }
    }
}

// Takes a string and split it into lines of at most `width` characters, ignoring
// ansi escape characters
fn manually_break_lines(s: &str, width: usize) -> Vec<String> {
    let (mut lines, last) = s
        .chars()
        .fold((vec![], String::new()), |(mut lines, mut line), c| {
            line.push(c);
            if measure_text_width(&line) >= width {
                lines.push(line);
                (lines, String::new())
            } else {
                (lines, line)
            }
        });
    lines.push(last);
    lines
}

// Runs a of 'commands' on each `payload` with a UI to print the status
// of the runs
pub async fn run_parallel_commands<T, F, Name, Cmd, Acc>(
    num_workers: usize,
    payloads: Vec<T>,
    runner: Cmd,
    namer: Name,
    ui_config: UiConfig,
) -> Result<Acc>
where
    Cmd: Fn(T, Arc<Mutex<String>>, Arc<Mutex<Acc>>) -> F,
    Name: Fn(&T) -> String,
    F: Future<Output = Result<CommandStatus>>,
    Acc: Default + Clone,
{
    // Create a list of work we need to perform from the payloads
    let work_list = payloads
        .into_iter()
        .map(|p| {
            (
                p,
                (
                    Arc::new(Mutex::new(CommandStatus::Waiting)),
                    Arc::new(Mutex::new(String::new())),
                ),
            )
        })
        .collect::<Vec<_>>();

    // Create a list of states we use to keep track of which commands are done
    let states = work_list
        .iter()
        .map(|(payload, (status, stdio))| TestState {
            name: namer(payload),
            status: status.clone(),
            stdio: stdio.clone(),
        })
        .collect::<Vec<_>>();

    // The work list needs to be mutex'd because we are goingto pop it in the workers
    let work_list = Arc::new(Mutex::new(work_list));

    // We need a way to specify to the UI task that it should exit early
    let (exit_tx, exit_rx) = oneshot::channel();
    // Spawn the UI task
    let thread_ui =
        tokio::spawn(async move { run_par_thread_ui(states, exit_rx, &ui_config).await });

    let result = Arc::new(Mutex::new(Default::default()));

    let mut workers = vec![];

    for _ in 0..num_workers {
        let worker = async {
            loop {
                let Some((payload, (test_status, stdio))) =
                    work_list.lock().await.pop()
                else {
                    break;
                };
                // Indicate to the UI that the command is running
                *test_status.lock().await = CommandStatus::Running;

                let status = runner(payload, stdio.clone(), result.clone()).await;

                match status {
                    Ok(status) => {
                        *test_status.lock().await = status;
                    }
                    Err(e) => {
                        *stdio.lock().await += &format!("{e:#?}");
                        *test_status.lock().await = CommandStatus::Failure;
                    }
                }
            }
            Ok::<(), color_eyre::eyre::Error>(())
        };
        workers.push(worker);
    }

    let worker_result = join_all(workers)
        .await
        .into_iter()
        .collect::<Result<Vec<_>>>();

    if worker_result.is_err() {
        exit_tx.send(()).unwrap();
        worker_result?;
    }

    thread_ui.await.unwrap().unwrap();
    let result = result.lock().await.clone();
    Ok(result)
}
