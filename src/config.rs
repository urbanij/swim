use std::borrow::Cow;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{anyhow, bail, Context, Result};
use log::warn;
use serde::Deserialize;

use std::collections::HashMap;

use crate::{
    libraries::{GitLibrary, Library, PathLibrary},
    nextpnr::{Ecp5Device, Ice40Device, NextpnrDevice},
    pnr::{PnrResult, PnrTool},
    util::pluralize,
};

pub fn default_simulator() -> String {
    "icarus".into()
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Simulation {
    /// Directory containing all test benches
    pub testbench_dir: Utf8PathBuf,
    /// `testbench_dir` but as originally specified in the toml file for easier-to-read testbench
    /// lists. Is skipped by serialization and initialized by the [Config::replace_relative_paths] function
    #[serde(skip)]
    pub testbench_dir_original: Utf8PathBuf,

    /// Extra dependencies to install to the test venv via pip
    pub python_deps: Option<Vec<String>>,

    /// The simulator to use. Currently verified to support verilator and icarus,
    /// but other simulators supported by cocotb may also work.
    ///
    /// Defaults to 'icarus'
    ///
    /// NOTE: Verilator must be exactly version 4.106
    /// See <https://docs.cocotb.org/en/stable/simulator_support.html> for more details
    /// NOTE: Verilator only supports synthesiseable verilog, which means that assertions
    /// will not be included
    #[serde(default = "default_simulator")]
    pub simulator: String,
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Synthesis {
    pub top: String,
    /// The yosys command to use for synthesis
    pub command: String,

    /// Extra verilog files only needed during the synthesis process
    pub extra_verilog: Option<Vec<Utf8PathBuf>>,
}

#[derive(Deserialize, Clone, Debug)]
pub struct NextPnrArgs<D> {
    pub device: D,
    pub package: String,
    #[serde(default)]
    pub allow_unconstrained: bool,
    #[serde(default)]
    pub allow_timing_fail: bool,
    /// ecp-5 uses lpf files while ice40 uses pcf files. This pin file
    /// argument is skipped by serde and filled in by `pnr_tool`
    #[serde(skip)]
    pub pin_file_arg: String,
    pub pin_file: Utf8PathBuf,
}

impl<D: 'static + NextpnrDevice> NextPnrArgs<D> {
    fn make_dyn(self, pin_format: &'static str) -> Result<NextPnrArgs<Box<dyn NextpnrDevice>>> {
        self.pin_file
            .extension()
            .ok_or(anyhow!(
                "The pin file must have an extension. Got {}",
                self.pin_file
            ))
            .and_then(|ext| {
                if ext != pin_format {
                    bail!(
                        "The pin file for this architecture must be {}. Got {}",
                        pin_format,
                        self.pin_file
                    )
                } else {
                    Ok(())
                }
            })?;

        Ok(NextPnrArgs {
            device: Box::new(self.device),
            allow_timing_fail: self.allow_timing_fail,
            package: self.package,
            allow_unconstrained: self.allow_unconstrained,
            pin_file: self.pin_file,
            pin_file_arg: format!("--{pin_format}"),
        })
    }
}

#[derive(Deserialize, Clone, Debug)]
#[serde(tag = "architecture")]
pub enum Pnr {
    #[serde(rename = "ice40")]
    Ice40(NextPnrArgs<Ice40Device>),
    #[serde(rename = "ecp5")]
    Ecp5(NextPnrArgs<Ecp5Device>),
}

impl Pnr {
    pub fn pnr_tool(&self) -> Result<PnrTool> {
        Ok(match self {
            Pnr::Ice40(args) => PnrTool::NextPnr {
                pnr_binary: Utf8PathBuf::from("nextpnr-ice40"),
                args: args.clone().make_dyn("pcf")?,
                output: PnrResult::Asc(Utf8PathBuf::from("hardware.asc")),
            },
            Pnr::Ecp5(args) => PnrTool::NextPnr {
                pnr_binary: Utf8PathBuf::from("nextpnr-ecp5"),
                args: args.clone().make_dyn("lpf")?,
                output: PnrResult::Textcfg(Utf8PathBuf::from("hardware.config")),
            },
        })
    }
}

#[derive(Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "tool")]
pub enum UploadTool {
    #[serde(rename = "icesprog")]
    Icesprog,
    #[serde(rename = "iceprog")]
    Iceprog,
    #[serde(rename = "tinyprog")]
    Tinyprog,
    #[serde(rename = "openocd")]
    OpenOcd { config_file: Utf8PathBuf },
    #[serde(rename = "fujprog")]
    Fujprog,
    #[serde(rename = "custom")]
    Custom { commands: Vec<String> },
}

impl UploadTool {
    pub fn binary(&self) -> &'static str {
        match self {
            UploadTool::Icesprog => "icesprog",
            UploadTool::Iceprog => "iceprog",
            UploadTool::Tinyprog => "tinyprog",
            UploadTool::OpenOcd { .. } => "openocd",
            UploadTool::Fujprog { .. } => "fujprog",
            UploadTool::Custom { .. } => panic!("Trying to get the binary of a custom command"),
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(tag = "name")]
pub enum Board {
    #[serde(rename = "ecpix-5", alias = "ecpix5")]
    Ecpix5 {
        pin_file: Option<Utf8PathBuf>,
        config_file: Option<Utf8PathBuf>,
    },
    #[serde(rename = "go-board", alias = "go", alias = "goboard")]
    GoBoard {
        pcf: Option<Utf8PathBuf>,
    },
    #[serde(rename = "tinyfpga-bx")]
    TinyfpgaBx {
        pcf: Option<Utf8PathBuf>,
    },
    Icestick {
        pcf: Option<Utf8PathBuf>,
    },
}

impl Board {
    fn upload(&self) -> Result<UploadTool> {
        match self {
            Board::Ecpix5 { config_file, .. } => config_file
                .as_ref()
                .cloned()
                .ok_or_else(|| {
                    anyhow!("field \"config_file\" required either in [board] or [upload]")
                })
                .map(|config_file| UploadTool::OpenOcd { config_file }),
            Board::GoBoard { .. } | Board::Icestick { .. } => Ok(UploadTool::Iceprog),
            Board::TinyfpgaBx { .. } => Ok(UploadTool::Tinyprog),
        }
    }

    fn pnr_ice40(
        &self,
        pcf: &Option<Utf8PathBuf>,
        package: &str,
        device: Ice40Device,
    ) -> Result<Pnr> {
        pcf.as_ref()
            .cloned()
            .ok_or_else(|| anyhow!("field \"pcf\" required either in [board] or [pnr]"))
            .map(|pcf| {
                Pnr::Ice40(NextPnrArgs {
                    device,
                    allow_timing_fail: false,
                    package: package.to_string(),
                    allow_unconstrained: false,
                    pin_file_arg: String::new(),
                    pin_file: pcf,
                })
            })
    }

    fn pnr(&self) -> Result<Pnr> {
        match self {
            Board::Ecpix5 { pin_file, .. } => pin_file
                .as_ref()
                .cloned()
                .ok_or_else(|| anyhow!("field \"pin_file\" required either in [board] or [pnr]"))
                .map(|pin_file| {
                    Pnr::Ecp5(NextPnrArgs {
                        device: Ecp5Device::Lfe5um5g45f,
                        package: "CABGA554".to_string(),
                        allow_unconstrained: false,
                        pin_file_arg: String::new(),
                        allow_timing_fail: false,
                        pin_file,
                    })
                }),
            Board::GoBoard { pcf } => self.pnr_ice40(pcf, "vq100", Ice40Device::Ice40hx1k),
            Board::TinyfpgaBx { pcf } => self.pnr_ice40(pcf, "cm81", Ice40Device::Ice40lp8k),
            Board::Icestick { pcf } => self.pnr_ice40(pcf, "tq144", Ice40Device::Ice40lp1k),
        }
    }

    fn packing(&self) -> Result<PackingTool> {
        match self {
            Board::Ecpix5 { .. } => Ok(PackingTool::Ecppack {
                idcode: Some("0x81112043".to_string()),
            }),
            Board::GoBoard { .. } | Board::TinyfpgaBx { .. } | Board::Icestick { .. } => {
                Ok(PackingTool::Icepack)
            }
        }
    }
}

#[derive(Deserialize, Clone, Debug)]
#[serde(tag = "tool")]
pub enum PackingTool {
    #[serde(rename = "icepack")]
    Icepack,
    #[serde(rename = "ecppack")]
    Ecppack { idcode: Option<String> },
}

impl PackingTool {
    pub fn binary(&self) -> &'static str {
        match self {
            PackingTool::Icepack => "icepack",
            PackingTool::Ecppack { .. } => "ecppack",
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "snake_case")]
pub enum LogOutputLevel {
    Full,
    Minimal,
}

impl Default for LogOutputLevel {
    fn default() -> Self {
        LogOutputLevel::Full
    }
}

pub fn default_compiler() -> Library {
    Library::Git(GitLibrary {
        url: "https://gitlab.com/spade-lang/spade.git".to_string(),
        branch: Some("master".to_string()),
        tag: None,
        commit: None,
    })
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Plugin {
    #[serde(flatten)]
    pub lib: Library,
    #[serde(default)]
    pub args: HashMap<String, String>,
}

/// The main project configuration specified in `swim.toml`
#[derive(Deserialize, Debug)]
pub struct Config {
    /// The name of the library. Must be a valid Spade identifier
    /// Anything defined in this library will be under the `name` namespace
    pub name: String,

    /// Where to find the Spade compiler. See [Library] for details
    #[serde(default = "default_compiler")]
    pub compiler: Library,

    /// List of commands to run before anything else.
    pub preprocessing: Option<Vec<String>>,

    /// Paths to verilog files to include in all verilog builds (simulation and synthesis)
    pub extra_verilog: Option<Vec<Utf8PathBuf>>,

    pub simulation: Option<Simulation>,

    pub synthesis: Option<Synthesis>,

    /// Preset board configuration which can be used instead of synthesis, pnr, packing and upload
    pub board: Option<Board>,

    pub pnr: Option<Pnr>,
    pub packing: Option<PackingTool>,
    pub upload: Option<UploadTool>,

    /// Map of libraries to include in the build.
    ///
    /// Example:
    /// ```toml
    /// [libraries]
    /// protocols = {git = https://gitlab.com/TheZoq2/spade_protocols.git}
    /// spade_v = {path = "deps/spade-v"}
    /// ```
    pub libraries: Option<HashMap<String, Library>>,

    /// Plugins to load. Specifies the location as a library, as well
    /// as arguments to the plugin
    ///
    /// Example:
    /// ```toml
    /// [plugins.loader_generator]
    /// path = "../plugins/loader_generator/"
    /// args.asm_file = "asm/blinky.asm"
    /// args.template_file = "../templates/program_loader.spade"
    /// args.target_file = "src/programs/blinky_loader.spade"
    ///
    /// [plugins.flamegraph]
    /// git = "https://gitlab.com/TheZoq2/yosys_flamegraph"
    /// ```
    ///
    /// Plugins contain a `swim_plugin.toml` which describes their behaviour.
    /// See [crate::plugin::config::PluginConfig] for details
    pub plugins: Option<HashMap<String, Plugin>>,

    #[serde(default)]
    pub log_output: LogOutputLevel,
}

macro_rules! config_getters {
    ($($fn:ident, $field:ident, $struct:ident);*$(;)?) => {
        $(
        pub fn $fn(&self) -> Result<&$struct> {
            get_field!(self, $field)
        }
        )*
    }
}

macro_rules! config_getters_with_board {
    ($($fn:ident, $field:ident, $struct:ident);*$(;)?) => {
        $(
        pub fn $fn(&self) -> Result<Cow<'_, $struct>> {
            match (&self.$field, &self.board) {
                (Some(_), _) => get_field!(self, $field).map(Cow::Borrowed),
                (None, Some(board)) => board.$field().map(Cow::Owned),
                (None, None) => bail!("either [board] or [{}] required", stringify!($field)),
            }
        }
        )*
    };
}

macro_rules! get_field {
    ($config:expr, $field:ident) => {
        $config.$field.as_ref().ok_or_else(|| {
            anyhow!(
                "swim.toml must have a [{}] section (or {} = ...) in order to run {}",
                stringify!($field),
                stringify!($field),
                stringify!($field)
            )
        })
    };
}

impl Config {
    config_getters! {
        simulation_config, simulation, Simulation;
        synthesis_config, synthesis, Synthesis;
    }

    config_getters_with_board! {
        packing_config, packing, PackingTool;
        pnr_config, pnr, Pnr;
        upload_config, upload, UploadTool;
    }

    pub fn read(root: impl AsRef<Utf8Path>, forced_compiler: &Option<Utf8PathBuf>) -> Result<Self> {
        let config_str = std::fs::read_to_string(root.as_ref().join("swim.toml"))
            .context("Failed to read swim.toml")?;
        let toml_de = toml::Deserializer::new(&config_str);
        let mut unknown_fields = std::collections::HashSet::new();
        let mut config: Config = serde_ignored::deserialize(toml_de, |path| {
            unknown_fields.insert(path.to_string());
        })
        .context("Failed to parse swim.toml")?;
        if !unknown_fields.is_empty() {
            warn!(
                "swim.toml contains {} unknown field{}:",
                unknown_fields.len(),
                pluralize(unknown_fields.len())
            );
            for field in &unknown_fields {
                warn!("  {}", field);
            }
        }
        if let Some(path) = forced_compiler {
            config.compiler = Library::Path(PathLibrary { path: path.clone() })
        }
        config.replace_relative_paths(root);
        Ok(config)
    }

    /// Replace all paths in the config with absolute paths.
    pub fn replace_relative_paths(&mut self, root: impl AsRef<Utf8Path>) {
        let root = root.as_ref();

        let Config {
            name: _,
            compiler,
            preprocessing: _,
            board,
            extra_verilog,
            simulation,
            synthesis,
            packing,
            pnr,
            upload,
            libraries,
            plugins,
            log_output: _,
        } = self;

        match compiler {
            Library::Git(_) => (),
            Library::Path(PathLibrary { path }) => join_if_relative(root, path),
        }

        match board {
            Some(Board::Ecpix5 {
                pin_file,
                config_file,
            }) => {
                join_if_relative(root, pin_file);
                join_if_relative(root, config_file);
            }
            Some(Board::GoBoard { pcf } | Board::TinyfpgaBx { pcf } | Board::Icestick { pcf }) => {
                join_if_relative(root, pcf);
            }
            None => (),
        }

        if let Some(extra_verilog) = extra_verilog {
            for path in extra_verilog {
                join_if_relative(root, path);
            }
        }

        if let Some(simulation) = simulation {
            simulation.testbench_dir_original = simulation.testbench_dir.clone();
            join_if_relative(root, &mut simulation.testbench_dir);
        }

        if let Some(synthesis) = synthesis {
            if let Some(extra_verilog) = &mut synthesis.extra_verilog {
                for file in extra_verilog {
                    join_if_relative(root, file);
                }
            }
        }

        match packing {
            Some(PackingTool::Icepack) => (),
            Some(PackingTool::Ecppack { idcode: _ }) => (),
            None => (),
        };

        match pnr {
            Some(Pnr::Ice40(args)) => join_if_relative(root, &mut args.pin_file),
            Some(Pnr::Ecp5(args)) => join_if_relative(root, &mut args.pin_file),
            None => (),
        }

        match upload {
            Some(
                UploadTool::Icesprog
                | UploadTool::Iceprog
                | UploadTool::Tinyprog
                | UploadTool::Fujprog,
            ) => (),
            Some(UploadTool::OpenOcd { config_file }) => join_if_relative(root, config_file),
            Some(UploadTool::Custom { .. }) => (),
            None => (),
        }

        if let Some(libraries) = libraries {
            for library in libraries.values_mut() {
                match library {
                    Library::Git(_) => (),
                    Library::Path(PathLibrary { path }) => join_if_relative(root, path),
                }
            }
        }
        if let Some(plugins) = plugins {
            for plugin in plugins.values_mut() {
                match &mut plugin.lib {
                    Library::Git(_) => (),
                    Library::Path(PathLibrary { path }) => join_if_relative(root, path),
                }
            }
        }
    }

    #[cfg(test)]
    pub(crate) fn new_empty(compiler: Library) -> Self {
        Self {
            name: "proj".to_string(),
            compiler,
            preprocessing: None,
            board: None,
            extra_verilog: None,
            simulation: None,
            synthesis: None,
            packing: None,
            pnr: None,
            upload: None,
            libraries: None,
            plugins: None,
            log_output: LogOutputLevel::Full,
        }
    }
}

/// If the given path is a relative path, join it with a prefix to make it absolute.
fn join_if_relative<'p>(root: &Utf8Path, path: impl Into<Option<&'p mut Utf8PathBuf>>) {
    assert!(root.is_absolute());

    if let Some(path) = path.into() {
        if path.is_relative() {
            let _ = std::mem::replace(path, root.join(&path));
        }
    }
}

#[cfg(test)]
mod tests {
    use camino::Utf8PathBuf;
    use std::collections::HashMap;

    use crate::config::Plugin;

    use super::{Config, Library, PathLibrary, Simulation, Synthesis, UploadTool};

    #[test]
    fn replaces_relative_dirs() {
        // NOTE: This config is deliberately made up.
        let mut config: Config = toml::from_str(&unindent::unindent(
            r#"
            name = "test"
            compiler = { path = "/my/own/spade" }
            extra_verilog = ["aaaa.v"]
            preprocessing = ["python3"]

            [simulation]
            testbench_dir = "tests"

            [synthesis]
            top = "bottom"
            command = "owo"
            extra_verilog = ["bbbb.v"]

            [upload]
            tool = "openocd"
            config_file = "ocd.conf"

            [libraries]
            lib = { path = "dem" }

            [plugins]
            plugin = { path = "dem" }
            "#,
        ))
        .unwrap();

        config.replace_relative_paths("/root/");

        let Config {
            name,
            compiler,
            preprocessing,
            board: _,
            extra_verilog,
            simulation,
            synthesis,
            packing: _,
            pnr: _,
            upload,
            libraries,
            plugins,
            log_output: _,
        } = config;

        assert_eq!(name, "test");

        assert_eq!(
            compiler,
            Library::Path(PathLibrary {
                path: Utf8PathBuf::from("/my/own/spade"),
            })
        );

        assert_eq!(extra_verilog, Some(vec![Utf8PathBuf::from("/root/aaaa.v")]));
        assert_eq!(preprocessing, Some(vec![String::from("python3")]));
        assert_eq!(
            simulation,
            Some(Simulation {
                testbench_dir: Utf8PathBuf::from("/root/tests"),
                testbench_dir_original: Utf8PathBuf::from("tests"),
                python_deps: None,
                simulator: "icarus".to_owned()
            }),
        );
        assert_eq!(
            synthesis,
            Some(Synthesis {
                top: "bottom".to_string(),
                command: "owo".to_string(),
                extra_verilog: Some(vec![Utf8PathBuf::from("/root/bbbb.v")]),
            }),
        );
        assert_eq!(
            upload,
            Some(UploadTool::OpenOcd {
                config_file: Utf8PathBuf::from("/root/ocd.conf")
            })
        );
        assert_eq!(
            libraries,
            Some(HashMap::from([(
                "lib".to_string(),
                Library::Path(PathLibrary {
                    path: Utf8PathBuf::from("/root/dem/"),
                })
            )])),
        );
        assert_eq!(
            plugins,
            Some(HashMap::from([(
                "plugin".to_string(),
                Plugin {
                    lib: Library::Path(PathLibrary {
                        path: Utf8PathBuf::from("/root/dem/"),
                    }),
                    args: HashMap::new()
                }
            )])),
        );
    }
}
