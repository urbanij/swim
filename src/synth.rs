use std::process::Command;

use camino::Utf8PathBuf;
use color_eyre::eyre::{anyhow, Context, Result};
use itertools::Itertools;
use log::{info, warn};

use crate::{
    build_dir,
    config::LogOutputLevel,
    plugin::config::BuildStep,
    report::yosys,
    spade::build_spade,
    util::{make_relative, needs_rebuild},
    yosys_stat_file, yosys_target_json, CommandCtx,
};

/// Synthesise the source files. Returns the path of the generated json
pub fn synthesise(ctx: &CommandCtx, custom_yosys_file: &Option<String>) -> Result<Utf8PathBuf> {
    let CommandCtx {
        root_dir,
        args: _,
        config,
        compiler: _,
        plugins,
    } = ctx;
    let spade_target = build_spade(ctx)?.verilog;

    let synth_config = config.synthesis_config()?;

    let global_extra_verilog = config.extra_verilog.clone().unwrap_or(vec![]);
    let synth_extra_verilog = synth_config.extra_verilog.clone().unwrap_or(vec![]);
    let sources = [make_relative(&spade_target)]
        .into_iter()
        .chain(global_extra_verilog.iter().map(make_relative))
        .chain(synth_extra_verilog.iter().map(make_relative))
        .collect::<Vec<_>>();

    let target_json = yosys_target_json(root_dir);

    let stat = yosys_stat_file(root_dir);

    let plugin_rebuild_triggers = plugins.rebuild_triggers(&BuildStep::Synthesis);

    if needs_rebuild(&target_json, &sources.clone())?
        || needs_rebuild(&target_json, &plugin_rebuild_triggers)?
        || custom_yosys_file.is_some()
    {
        info!("Synthesizing");

        let log = build_dir(root_dir).join("yosys.log");

        let yosys_postprocessing = plugins
            .inner
            .iter()
            .flat_map(|(_, p)| {
                p.synthesis
                    .as_ref()
                    .map_or(vec![], |s| s.yosys_post.clone())
            })
            .join("\n");

        let yosys_command_string = if let Some(command_file) = custom_yosys_file {
            let file_commands = std::fs::read_to_string(&command_file)
                .with_context(|| format!("Failed to read custom commands from {command_file}"))?;
            format!(
                r#"
                read_verilog -sv {sources};
                # read_verilog -lib -specify +/ecp5/cells_sim.v +/ecp5/cells_bb.v
                hierarchy -check -top {top}
                {file_commands}
                "#,
                sources = sources.iter().join(" "),
                top = synth_config.top,
            )
        } else {
            format!(
                r#"
                read_verilog -sv {sources};
                {synth_command} -top {top} -json {target_json};
                tee -q -o {stat} stat;
                {yosys_postprocessing}
                "#,
                sources = sources.iter().join(" "),
                synth_command = synth_config.command,
                top = synth_config.top,
            )
        };

        let mut command = Command::new("yosys");

        // Redirect output if minimal output level
        if matches!(config.log_output, LogOutputLevel::Minimal) {
            let yosys_log =
                std::fs::File::create(&log).with_context(|| "Failed to create yosys log output")?;
            command.stdout(yosys_log);
        }

        let status = command
            .arg("-p")
            .arg(yosys_command_string)
            .status()
            .context("Failed to run yosys")?;

        if !status.success() {
            let err_msg = format!(
                "Synthesis failed{}",
                if matches!(config.log_output, LogOutputLevel::Minimal) {
                    format!(". See {} for information", log)
                } else {
                    String::new()
                }
            );
            return Err(anyhow!(err_msg));
        }

        info!("Synthesised {}", target_json);
    } else {
        info!("{} is up to date", target_json);
    }

    // Only print a report if we run the default flow
    if custom_yosys_file.is_none() {
        let stat_output = std::fs::read_to_string(&stat)
            .with_context(|| format!("Failed to read {}", stat))
            .map(|s| yosys::parse_stat_command(&s));

        match stat_output {
            Ok(output) => info!("yosys synthesis report:\n{output}"),
            Err(e) => warn!("Error parsing yosys stat: {}", e),
        }
    }

    Ok(target_json)
}
