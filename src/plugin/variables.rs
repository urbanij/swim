use std::collections::HashMap;

use camino::Utf8PathBuf;
use color_eyre::{
    eyre::{anyhow, Context},
    Result,
};
use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;

use crate::{compiler_state_file, config::Config, test_list_file};

pub struct PluginContext {
    /// The directory containing the plugin itself
    pub plugin_dir: Utf8PathBuf,
    /// The arguemnts passed to the plugin
    pub args: HashMap<String, String>,

    pub root_dir: Utf8PathBuf,
    pub compiler_dir: Utf8PathBuf,
    pub build_dir: Utf8PathBuf,
}

fn variable_mapping() -> HashMap<String, Box<dyn Fn(&PluginContext, &Config) -> Result<String>>> {
    vec![
        (
            "PLUGIN_DIR",
            Box::new(|p: &PluginContext, _: &Config| Ok(p.plugin_dir.to_string())) as Box<_>,
        ),
        (
            "COMPILER_DIR",
            Box::new(|p: &PluginContext, _: &Config| Ok(p.compiler_dir.to_string())) as Box<_>,
        ),
        (
            "BUILD_DIR",
            Box::new(|p: &PluginContext, _: &Config| Ok(p.build_dir.to_string())) as Box<_>,
        ),
        (
            "PROJECT_ROOT",
            Box::new(|p: &PluginContext, _: &Config| Ok(p.root_dir.to_string())) as Box<_>,
        ),
        (
            "SYNTHESIS_TOP",
            Box::new(|_: &PluginContext, c: &Config| Ok(c.synthesis_config()?.top.clone()))
                as Box<_>,
        ),
        (
            "COMPILER_STATE_FILE",
            Box::new(|p: &PluginContext, _: &Config| {
                Ok(compiler_state_file(&p.root_dir).to_string())
            }) as Box<_>,
        ),
        (
            "TEST_LIST_FILE",
            Box::new(|p: &PluginContext, _: &Config| Ok(test_list_file(&p.root_dir).to_string()))
                as Box<_>,
        ),
    ]
    .into_iter()
    .map(|(v, f)| (v.to_string(), f))
    .collect()
}

/// Replaces variables in the specified string. Errors if an unknown variable is used
pub fn translate_variables(
    s: &mut String,
    plugin_ctx: &PluginContext,
    config: &Config,
) -> Result<()> {
    // Ensure that all arguments required by the plugin are passed

    let mapping = variable_mapping();

    lazy_static! {
        static ref VAR_RE: Regex = Regex::new(r"\$\{([A-z_0-9]+)\}").unwrap();
    }

    while let Some(caps) = VAR_RE.captures(s) {
        let var = &caps[1];

        let mapped = mapping
            .get(&var.to_string())
            .map(|f| f(plugin_ctx, config).with_context(|| format!("When evaluating {var}")))
            .transpose()?
            .or_else(|| plugin_ctx.args.get(&var.to_string()).cloned())
            .ok_or_else(|| {
                let alternatives = mapping.keys().chain(plugin_ctx.args.keys()).join("\t\n");
                anyhow!("{var} is not a known variable in plugin config. Available variables: \t\n{alternatives}")
            })?;

        *s = VAR_RE
            .replace(s, |_: &regex::Captures| mapped.clone())
            .to_string();
    }

    Ok(())
}
