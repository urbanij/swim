use std::process::Command;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{bail, Context, Result};
use log::{debug, info};

use crate::{
    config::Config,
    libraries::{
        content_redirect, load_git_library, Library, LockFile, LockFileRef, RestoreAction,
    },
    lock_file, plugin_dir,
};

use self::config::{BuildStep, PluginCommand, PluginConfig};

pub mod config;
pub mod variables;

#[derive(Debug)]
pub struct PluginList {
    pub inner: Vec<(String, PluginConfig)>,
}

impl PluginList {
    pub fn new(inner: Vec<(String, PluginConfig)>) -> Self {
        Self { inner }
    }

    #[cfg(test)]
    pub(crate) fn empty() -> PluginList {
        Self { inner: vec![] }
    }

    /// Runs all the preprocessing commands required by the list of plugins
    pub fn run_preprocessing_commands(&self) -> Result<()> {
        // Run the preprocessing commands for all the plugins
        for (name, plugin) in &self.inner {
            run_command_list(name, &plugin.build_commands, "preprocessing").with_context(|| {
                format!("When running preprocessing commands for plugin {name}")
            })?;
        }
        Ok(())
    }

    pub fn run_post_build_commands(&self) -> Result<()> {
        for (name, cfg) in &self.inner {
            run_command_list(name, &cfg.post_build_commands, "post build")
                .with_context(|| format!("When running post build commands for plugin {name}"))?;
        }

        Ok(())
    }

    pub fn rebuild_triggers(&self, step: &BuildStep) -> Vec<Utf8PathBuf> {
        self.inner
            .iter()
            .flat_map(|(_name, cfg)| cfg.rebuild_list(step))
            .collect::<Vec<_>>()
    }

    pub fn commands_matching(
        &self,
        target: &str,
    ) -> Vec<(&(String, PluginConfig), &PluginCommand)> {
        self.inner
            .iter()
            .filter_map(|plugin| {
                plugin
                    .1
                    .commands
                    .iter()
                    .find(|(cmd_name, _)| cmd_name.as_str() == target)
                    .map(|(_, cmd)| (plugin, cmd))
            })
            .collect()
    }
}

pub fn load_plugin(
    plugin_dir: &Utf8Path,
    name: &str,
    library: &Library,
    lock_file: &mut LockFileRef,
    restore_action: RestoreAction,
) -> Result<Utf8PathBuf> {
    let dir = plugin_dir.join(name);

    match library {
        Library::Git(git_library) => load_git_library(
            git_library,
            &dir,
            name,
            lock_file,
            |l| &mut l.plugins,
            restore_action,
        )?,
        // Do nothing for path plugins since the source is already on disk by definition.
        Library::Path(_) => (),
    }

    let lib_content_dir = content_redirect(library).unwrap_or(&dir);

    Ok(lib_content_dir.to_owned())
}

pub fn load_plugins(
    root_dir: &Utf8Path,
    config: &Config,
    restore_action: RestoreAction,
) -> Result<PluginList> {
    let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
    let plugin_dir = plugin_dir(root_dir);

    // Make sure the plugin exists on disk.
    //
    // For example, this clones any non-existing git repositories. It does not update them if they
    // already exist.
    let plugins = config
        .plugins
        .iter()
        .flatten()
        .map(|(name, plugin)| {
            let actual_dir = load_plugin(
                &plugin_dir,
                name,
                &plugin.lib,
                &mut lock_file,
                restore_action,
            )?;

            let loaded = PluginConfig::load(name, &actual_dir, root_dir, config, &plugin.args)?;

            Ok((name.clone(), loaded))
        })
        .collect::<Result<Vec<_>>>()?;

    if let Some(plugins) = &mut lock_file.inner.plugins {
        plugins.filter_unseen();
    }

    Ok(PluginList::new(plugins))
}

pub fn restore_plugins(
    root_dir: &Utf8Path,
    config: &Config,
    lock_file: &mut LockFileRef,
) -> Result<()> {
    config
        .plugins
        .iter()
        .flatten()
        .try_for_each(|(name, plugin)| {
            load_plugin(
                &plugin_dir(root_dir),
                name,
                &plugin.lib,
                lock_file,
                RestoreAction::Restore,
            )?;
            Ok(())
        })
}

pub fn run_command_list(plugin: &str, commands: &[String], step: &str) -> Result<()> {
    if commands.is_empty() {
        debug!("No commands for {step}");
    } else {
        info!("Running {step} commands for {plugin}:");
    }

    for command in commands {
        let status = Command::new("sh")
            .arg("-c")
            .arg("--")
            .arg(command)
            .status()
            .with_context(|| format!("Failed to run {command}"))?;

        if !status.success() {
            bail!("Command {command} exited with error code")
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use crate::{
        cmdline::Command,
        config::Config,
        libraries::RestoreAction,
        plugin::load_plugins,
        spade,
        test::{self, init_plugin},
        CommandCtx,
    };

    #[test]
    fn plugins_work_and_execute() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir: _,
            spade_target: _,
        } = test::get_compiler();

        let (project, config) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                entity top() -> bool {
                    false
                }
            ",
            ),
            &unindent::unindent(
                r#"
                name = "proj"
                [plugins]
                test_plugin = { path = "test_plugin" }
            "#,
            ),
        );

        let plugin_config = unindent::unindent(
            r#"
            build_commands = ["sh -c 'echo \"plugin result\" > ${BUILD_DIR}/plugin_out'"]
            builds = []
            "#,
        );

        test::init_plugin(
            &project.root_utf8.join("test_plugin"),
            &plugin_config,
            vec![],
        );

        let plugins = load_plugins(&project.root_utf8, &config, RestoreAction::Deny).unwrap();
        plugins.run_preprocessing_commands().unwrap();

        let content = std::fs::read_to_string(project.root_utf8.join("build/plugin_out")).unwrap();
        assert_eq!(content, "plugin result\n");
    }

    mod deny_changes_to_plugins {
        use crate::{
            config::Config,
            libraries::{LockFile, RestoreAction},
            plugin::{load_plugins, restore_plugins},
            plugin_dir,
            test::{self, init_plugin, Project},
        };

        fn prepare() -> (Project, Config) {
            let (project, _) = test::init_spade_repo_with(
                &unindent::unindent(
                    "
                    entity top() -> bool {
                        false
                    }
                ",
                ),
                r#"name = "proj""#,
            );

            let plugin_dir = project.root_utf8.join("test_plugin");
            let plugin_config = unindent::unindent(
                r#"
                build_commands = ["sh -c 'echo \"plugin result\" > ${BUILD_DIR}/plugin_out'"]
                builds = []
                "#,
            );
            init_plugin(&plugin_dir, &plugin_config, vec![]);

            std::fs::write(
                &project.config,
                &unindent::unindent(&format!(
                    r#"
                    name = "proj"

                    [plugins]
                    test_plugin = {{ git = "file://{plugin_dir}", branch = "master" }}
                "#
                )),
            )
            .unwrap();
            let config = Config::read(&project.root_utf8, &None).unwrap();

            (project, config)
        }

        fn verify(project: &Project, config: &Config) {
            let plugins = load_plugins(&project.root_utf8, config, RestoreAction::Deny).unwrap();
            plugins.run_preprocessing_commands().unwrap();
            let content =
                std::fs::read_to_string(project.root_utf8.join("build/plugin_out")).unwrap();
            assert_eq!(content, "plugin result\n");
        }

        #[test]
        fn edits_are_denied() {
            let (project, config) = prepare();

            verify(&project, &config);

            let new_plugin_config = unindent::unindent(
                r#"
                name = "proj"
                build_commands = ["sh -c 'echo \"plugin result 2 electric boogaloo\" > ${BUILD_DIR}/plugin_out'"]
                builds = []
                "#,
            );
            std::fs::write(
                plugin_dir(&project.root_utf8).join("test_plugin/swim_plugin.toml"),
                new_plugin_config,
            )
            .unwrap();
            let _ = load_plugins(&project.root_utf8, &config, RestoreAction::Deny)
                .map(|_| ())
                .unwrap_err();
        }

        #[test]
        fn restores_work() {
            let (project, config) = prepare();

            verify(&project, &config);

            let new_plugin_config = unindent::unindent(
                r#"
                name = "proj"
                build_commands = ["sh -c 'echo \"plugin result 2 electric boogaloo\" > ${BUILD_DIR}/plugin_out'"]
                builds = []
                "#,
            );
            std::fs::write(
                plugin_dir(&project.root_utf8).join("test_plugin/swim_plugin.toml"),
                new_plugin_config,
            )
            .unwrap();
            let _ = load_plugins(&project.root_utf8, &config, RestoreAction::Deny)
                .map(|_| ())
                .unwrap_err();

            let mut lock_file = LockFile::try_open(&project.lock_file).unwrap();
            restore_plugins(&project.root_utf8, &config, &mut lock_file).unwrap();

            verify(&project, &config);
        }
    }

    #[test]
    fn plugin_post_build_commands_run() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir: _,
            spade_target: _,
        } = test::get_compiler();

        let (project, _) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                    entity top() -> bool {
                        false
                    }
                ",
            ),
            r#"name = "proj""#,
        );

        let plugin_dir = project.root_utf8.join("test_plugin");
        let plugin_config = unindent::unindent(
            r#"
                build_commands = []
                post_build_commands = ["sh -c 'echo \"plugin result\" > ${BUILD_DIR}/plugin_out'"]
                builds = []
                "#,
        );
        init_plugin(&plugin_dir, &plugin_config, vec![]);

        std::fs::write(
            &project.config,
            &unindent::unindent(&format!(
                r#"
                    name = "proj"
                    [plugins]
                    test_plugin = {{ path = "{plugin_dir}" }}
                "#
            )),
        )
        .unwrap();
        let config = Config::read(&project.root_utf8, &None).unwrap();

        test::init_plugin(&plugin_dir, &plugin_config, vec![]);
        let plugins = load_plugins(&project.root_utf8, &config, RestoreAction::Deny).unwrap();
        dbg!(&plugins);

        let test::SpadeDirs {
            compiler_dir: _,
            spade_target,
        } = test::get_compiler();
        let args = Command::Build.into_args();

        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins,
        };
        let _ = spade::build_spade(&ctx).unwrap();

        let content = std::fs::read_to_string(project.root_utf8.join("build/plugin_out")).unwrap();
        assert_eq!(content, "plugin result\n");
    }
}
