use std::collections::BTreeMap;
use std::process::Command;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{anyhow, bail, Context, Result};
use log::{debug, info};
use serde::{Deserialize, Serialize};

use crate::{
    config::Config,
    libs_dir, src_dir,
    util::{make_relative, CommandExt, ExitStatusExt, OutputExt},
};

#[derive(Clone, Copy, Debug)]
pub enum RestoreAction {
    Deny,
    Restore,
}

/// Location of a library or external code. Either a link to a git repository, or
/// a path relative to the root of the project.
///
/// ```toml
/// compiler = {git = "https://gitlab.com/spade-lang/spade/"}
/// ```
///
/// ```toml
/// path = "compiler/"
/// ```
#[derive(Deserialize, Clone, Debug, PartialEq)]
#[serde(untagged)]
pub enum Library {
    Git(GitLibrary),
    Path(PathLibrary),
}

#[derive(Deserialize, Clone, Debug, PartialEq)]
pub struct GitLibrary {
    #[serde(rename = "git")]
    pub url: String,
    pub commit: Option<String>,
    pub tag: Option<String>,
    pub branch: Option<String>,
}

#[derive(Deserialize, Clone, Debug, PartialEq)]
pub struct PathLibrary {
    pub path: Utf8PathBuf,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct LockFileLibrary {
    commit: String,
    /// Used to filter out libraries that have been removed.
    #[serde(skip, default)]
    seen: bool,
}

impl LockFileLibrary {
    pub fn new(commit: String) -> Self {
        Self {
            commit,
            seen: false,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct LockFileSection(pub BTreeMap<String, LockFileLibrary>);

impl LockFileSection {
    /// Filter out any entries we haven't seen since opening the lock file.
    ///
    /// This should be called after something has gone through the entries
    /// in the section, since otherwise it will just remove everything.
    pub fn filter_unseen(&mut self) {
        self.0.retain(|_, library| library.seen);
    }
}

#[derive(Debug, Default)]
pub struct LockFileRef {
    pub inner: LockFile,
    path: Utf8PathBuf,
}

/// Representation of swim.lock.
#[derive(Deserialize, Serialize, Debug, Default)]
pub struct LockFile {
    pub libraries: Option<LockFileSection>,
    pub plugins: Option<LockFileSection>,
    pub spade: Option<LockFileLibrary>,
}

impl LockFile {
    /// Try to open the lock file at the specified path. If it fails, return a new empty lock file.
    pub fn open_or_default(path: impl AsRef<Utf8Path>) -> LockFileRef {
        Self::try_open(&path).unwrap_or_else(|_| LockFileRef {
            inner: LockFile::default(),
            path: path.as_ref().to_path_buf(),
        })
    }

    pub fn try_open(path: impl AsRef<Utf8Path>) -> Result<LockFileRef> {
        let path = path.as_ref();
        let content =
            std::fs::read_to_string(path).with_context(|| format!("When reading {path:?}"))?;
        let inner = toml::from_str(&content).with_context(|| format!("When parsing {path:?}"))?;
        Ok(LockFileRef {
            inner,
            path: path.to_path_buf(),
        })
    }
}

impl LockFileRef {
    pub fn try_write(&mut self) -> Result<()> {
        // If any section is empty, replace with None
        macro_rules! set_none_if_empty_inside {
            ($section:expr) => {
                if $section.as_ref().map(|section| section.0.is_empty()) == Some(true) {
                    *$section = None;
                }
            };
        }

        let LockFile {
            libraries,
            plugins,
            spade: _,
        } = &mut self.inner;

        set_none_if_empty_inside!(libraries);
        set_none_if_empty_inside!(plugins);

        let content = toml::to_string(&self.inner)?;
        std::fs::write(&self.path, &content)
            .with_context(|| format!("When writing to {:?}", self.path))?;
        Ok(())
    }
}

impl Drop for LockFileRef {
    fn drop(&mut self) {
        debug!("Dropping lock file {self:?}");
    }
}

/// The directory that contains the actual content for the library.
/// For example, a git library is cloned into `dir`, while a path library
/// actually has its contents somewhere else. The path library still gets its own place
/// in `dir` but the source ("content") won't be there.
pub fn content_redirect(lib: &Library) -> Option<&Utf8Path> {
    match lib {
        Library::Git(_) => None,
        Library::Path(PathLibrary { path }) => Some(path),
    }
}

#[derive(Debug)]
pub enum GitRef {
    Commit(String),
    Tag(String),
    Branch(String),
}

impl GitRef {
    pub fn parse_git_library(library: GitLibrary) -> Result<Self> {
        let GitLibrary {
            url: _,
            mut commit,
            mut tag,
            mut branch,
        } = library;

        let num_specified = [&commit, &tag, &branch]
            .iter()
            .filter(|o| o.is_some())
            .count();
        if num_specified == 0 {
            bail!("At least one of 'commit', 'tag' or 'branch' must be specified");
        } else if num_specified > 1 {
            bail!("Only one of 'commit', 'tag' or 'branch' can be specified");
        }

        if let Some(commit) = commit.take() {
            Ok(GitRef::Commit(commit))
        } else if let Some(tag) = tag.take() {
            Ok(GitRef::Tag(tag))
        } else if let Some(branch) = branch.take() {
            Ok(GitRef::Branch(branch))
        } else {
            unreachable!("one ref should have been found");
        }
    }
}

fn clone(dir: &Utf8Path, url: &str) -> Result<()> {
    // Make sure the directory exists.
    std::fs::create_dir_all(dir)?;

    Command::new("git")
        .current_dir(dir)
        .args(["clone", "--origin", "origin", url, dir.as_str()])
        .log_command()
        .status_and_log_output_if_tests()?
        .success_or_else(|| anyhow!("Failed to clone repository"))
}

fn fetch(dir: &Utf8Path) -> Result<()> {
    Command::new("git")
        .current_dir(dir)
        .args(["fetch", "--all", "--tags"])
        .log_command()
        .status_and_log_output_if_tests()?
        .success_or_else(|| anyhow!("Failed to fetch repository"))
}

fn checkout_force(dir: &Utf8Path, gitref: &GitRef) -> Result<()> {
    Command::new("git")
        .current_dir(dir)
        .args(["checkout", "--detach"])
        .args(vec!["--force"])
        .arg(match gitref {
            GitRef::Commit(commit) => commit.to_string(),
            // Disambiguate branches and tags, just to be sure.
            GitRef::Branch(branch) => format!("refs/remotes/origin/{branch}"),
            GitRef::Tag(tag) => format!("refs/tags/{tag}"),
        })
        .arg("--")
        .log_command()
        .status_and_log_output_if_tests()?
        .success_or_else(|| anyhow!("Failed to checkout {:?}", gitref))
}

fn git_contains_changes(dir: &Utf8Path) -> Result<bool> {
    let status = Command::new("git")
        .current_dir(dir)
        .args(["diff", "--exit-code"])
        .log_command()
        .status_and_log_output()?;
    Ok(!status.success())
}

fn git_restore_changes(dir: &Utf8Path) -> Result<()> {
    Command::new("git")
        .current_dir(dir)
        .args(["restore", "."])
        .log_command()
        .status_and_log_output_if_tests_or_errs()?
        .success_or_else(|| anyhow!("Failed to restore files"))
}

fn current_commit(dir: &Utf8Path) -> Result<String> {
    Command::new("git")
        .current_dir(dir)
        .args(["rev-parse", "HEAD"])
        .log_command()
        .output_and_log_output()
        .map(|output| output.stdout_str().as_ref().trim().to_string())
        .map_err(|e| e.into())
}

pub(crate) fn update_git_library(
    git_library: &GitLibrary,
    dir: &Utf8Path,
    name: &str,
    lock_file: &mut LockFileRef,
) -> Result<()> {
    let commit = get_git_library_commit(git_library, dir, name)?;
    lock_file
        .inner
        .libraries
        .get_or_insert_with(LockFileSection::default)
        .0
        .insert(name.to_string(), LockFileLibrary { commit, seen: true });

    Ok(())
}

pub(crate) fn update_spade_git_library(
    git_library: &GitLibrary,
    dir: &Utf8Path,
    lock_file: &mut LockFileRef,
) -> Result<()> {
    let commit = get_git_library_commit(git_library, dir, "spade")?;
    lock_file.inner.spade = Some(LockFileLibrary { commit, seen: true });
    Ok(())
}

fn get_git_library_commit(git_library: &GitLibrary, dir: &Utf8Path, name: &str) -> Result<String> {
    if !dir.exists() {
        clone(dir, &git_library.url).with_context(|| {
            format!(
                "While cloning {:?} from {:?} into {:?}",
                name, git_library.url, dir
            )
        })?;
    } else {
        fetch(dir).with_context(|| format!("While fetching dependency {name:?} in {dir:?}"))?;
    }

    let gitref = GitRef::parse_git_library(git_library.clone())
        .with_context(|| format!("In config for {name:?}"))?;
    if git_contains_changes(dir)? {
        bail!("{name} ({dir}) contains modifications");
    }
    checkout_force(dir, &gitref)
        .with_context(|| format!("While checking out {gitref:?} in {name:?} in {dir:?}"))?;

    current_commit(dir).with_context(|| format!("Checking for the current commit in {dir:?}"))
}

/// Update all libraries by fetching from the network, checking out new commits and updating the
/// lockfile.
pub fn update_libraries(
    lib_dir: &Utf8Path,
    config: &Config,
    lock_file: &mut LockFileRef,
) -> Result<()> {
    for (name, library) in config.libraries.iter().flatten() {
        info!("Updating dependency {name:?}");
        let dir = lib_dir.join(name);

        match library {
            Library::Git(git_library) => update_git_library(git_library, &dir, name, lock_file)?,
            Library::Path(_) => (),
        }

        let lib_content_dir = content_redirect(library).unwrap_or(&dir);

        let lib_config = Config::read(lib_content_dir, &None)
            .with_context(|| format!("In dependency {name:?}"))?;
        let lib_build_dir = &libs_dir(lib_content_dir);

        update_libraries(lib_build_dir, &lib_config, lock_file)?;
    }

    Ok(())
}

/// Return the library's commit from the lock file if it exists there, otherwise
/// from the passed library. Also checkouts the commit respecting the passed
/// [RestoreAction].
fn get_commit_and_checkout_from_lock_or_library(
    entry: Option<&mut LockFileLibrary>,
    name: &str,
    dir: &Utf8Path,
    library: &GitLibrary,
    restore_action: RestoreAction,
) -> Result<String> {
    let gitref = if let Some(locked) = entry {
        debug!("Exists in lock file");
        locked.seen = true;
        GitRef::Commit(locked.commit.to_string())
    } else {
        GitRef::parse_git_library((*library).clone())
            .with_context(|| format!("In config for {name:?}"))?
    };

    if git_contains_changes(dir)? {
        match restore_action {
            RestoreAction::Deny => bail!("{name} ({dir}) contains modifications"),
            RestoreAction::Restore => git_restore_changes(dir)?,
        }
    }
    checkout_force(dir, &gitref).with_context(|| {
        format!("Running checkout {gitref:?} from swim.lock in {name:?} in {dir:?}")
    })?;

    current_commit(dir).with_context(|| format!("Checking for the current commit in {dir:?}"))
}

fn clone_if_not_exists(name: &str, dir: &Utf8Path, library: &GitLibrary) -> Result<()> {
    if !dir.exists() {
        info!("Cloning {name:?}");
        clone(dir, &library.url).with_context(|| {
            format!(
                "Cloning {:?} from {:?} into {:?} since it wasn't found locally",
                name, library.url, dir,
            )
        })?;
    }
    Ok(())
}

pub(crate) fn load_spade_library(
    library: &GitLibrary,
    dir: &Utf8Path,
    lock_file: &mut LockFileRef,
    restore_action: RestoreAction,
) -> Result<()> {
    let name = "spade";

    clone_if_not_exists(name, dir, library)?;

    let commit = get_commit_and_checkout_from_lock_or_library(
        lock_file.inner.spade.as_mut(),
        name,
        dir,
        library,
        restore_action,
    )?;

    let _ = lock_file
        .inner
        .spade
        .insert(LockFileLibrary { commit, seen: true });

    Ok(())
}

pub(crate) fn load_git_library<F>(
    library: &GitLibrary,
    dir: &Utf8Path,
    name: &str,
    lock_file: &mut LockFileRef,
    lock_file_section: F,
    restore_action: RestoreAction,
) -> Result<()>
where
    F: Fn(&mut LockFile) -> &mut Option<LockFileSection>,
{
    clone_if_not_exists(name, dir, library)?;

    let commit = get_commit_and_checkout_from_lock_or_library(
        lock_file_section(&mut lock_file.inner)
            .get_or_insert_with(LockFileSection::default)
            .0
            .get_mut(name),
        name,
        dir,
        library,
        restore_action,
    )?;

    lock_file_section(&mut lock_file.inner)
        .get_or_insert_with(LockFileSection::default)
        .0
        .insert(name.to_string(), LockFileLibrary { commit, seen: true });

    Ok(())
}

pub fn load_library(
    lib_dir: &Utf8Path,
    name: &str,
    library: &Library,
    lock_file: &mut LockFileRef,
    restore_action: RestoreAction,
) -> Result<Vec<(String, Utf8PathBuf)>> {
    let dir = lib_dir.join(name);

    // Make sure the library exists on disk.
    //
    // For example, this clones any non-existing git repositories. It does not update them if they
    // already exist.
    match library {
        Library::Git(git_library) => load_git_library(
            git_library,
            &dir,
            name,
            lock_file,
            |l| &mut l.libraries,
            restore_action,
        )?,
        // Do nothing for path dependencies since the source is already on disk by definition.
        Library::Path(_) => (),
    }

    // Where the library is located.
    let lib_content_dir = content_redirect(library).unwrap_or(&dir);

    let lib_config =
        Config::read(lib_content_dir, &None).with_context(|| format!("In dependency {name:?}"))?;

    if lib_config.name != name {
        let msg = [
            format!(
                "Library name missmatch. Expected {} got {}.",
                lib_config.name, name
            ),
            format!(
                "   {} is specified in {}",
                lib_config.name,
                make_relative(&lib_content_dir).join("swim.toml")
            ),
            format!("   {name} is specified in the local \"swim.toml\""),
        ]
        .join("\n");
        bail!("{msg}")
    }

    // Where to put this library's dependencies.
    //
    // NOTE: This is /not/ always in the content dir. For example, a path dependency has its source
    // somewhere else but should be built in "our" build/. See also the reasoning on
    // [content_redirect].
    let lib_build_dir = libs_dir(lib_content_dir);

    // Get the source dirs of this library's own libraries.
    // The vec is empty if this library doesn't contain any libraries.
    let mut source_dirs = load_libraries(&lib_build_dir, &lib_config, lock_file, restore_action)?;

    // Also add this library's source dir.
    source_dirs.push((name.to_string(), src_dir(lib_content_dir)));

    Ok(source_dirs)
}

/// Returns a list of names of and paths to available libraries.
///
/// Notably, this is _not_ just the directory containg the external libraries. We don't want to
/// have to remove directories when the dependency is removed, in case it is used later.
///
/// If a dependency doesn't have a directory locally, either because it's a new dependency or
/// because it was removed, this will try to fetch it from the network.
///
/// If the lock file specifies a commit, that will be used instead.
pub fn load_libraries(
    lib_dir: &Utf8Path,
    config: &Config,
    lock_file: &mut LockFileRef,
    restore_action: RestoreAction,
) -> Result<Vec<(String, Utf8PathBuf)>> {
    config
        .libraries
        .iter()
        .flatten()
        .map(|(name, library)| load_library(lib_dir, name, library, lock_file, restore_action))
        .collect::<Result<Vec<Vec<_>>>>()
        .map(|v| v.into_iter().flatten().collect())
}

/// For all libraries that are git dependencies managed by swim, restore the
/// local repository to the commit specified in swim.lock, or swim.toml if
/// swim.lock doesn't exist.
pub fn restore_libraries(
    root_dir: &Utf8Path,
    config: &Config,
    lock_file: &mut LockFileRef,
) -> Result<()> {
    config
        .libraries
        .iter()
        .flatten()
        .try_for_each(|(name, library)| {
            load_library(
                &libs_dir(root_dir),
                name,
                library,
                lock_file,
                RestoreAction::Restore,
            )?;
            Ok(())
        })
}

#[cfg(test)]
mod tests {
    use super::update_libraries;

    use crate::cmdline::Command;
    use crate::config::Config;
    use crate::libraries::LockFile;
    use crate::plugin::PluginList;
    use crate::spade;
    use crate::test;
    use crate::CommandCtx;

    use assert_fs::prelude::*;
    use camino::Utf8Path;

    #[test]
    fn building_with_path_dependencies_work() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir: _,
            spade_target,
        } = test::get_compiler();

        let (dependency, _) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                fn two() -> int<4> {
                    2
                }
            ",
            ),
            r#"name = "dependency""#,
        );

        // Use the same function in the project.
        let (project, config) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                entity top() -> bool {
                    dependency::main::two() == 2
                }
            ",
            ),
            &unindent::unindent(&format!(
                r#"
                name = "proj"
                [libraries]
                dependency = {{ path = "{}" }}
            "#,
                dependency.root_utf8
            )),
        );

        let args = Command::Build.into_args();
        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        let _ = spade::build_spade(&ctx).unwrap();
    }

    fn setup_git_dependency_project(
        compiler_dir: &Utf8Path,
    ) -> ((test::Project, Config), test::Project) {
        let (dependency, _) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                fn two() -> int<4> {
                    2
                }
            ",
            ),
            r#"name = "dependency""#,
        );
        test::git::create_branch(&dependency.root, "a-branch");
        test::git::add(&dependency.root, &dependency.source_files);
        test::git::add(&dependency.root, [&dependency.config]);
        test::git::commit(&dependency.root, "initial commit");

        // Use the same function in the project.
        let (project, config) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                entity top() -> bool {
                    dependency::main::two() == 2
                }
            ",
            ),
            &unindent::unindent(&format!(
                r#"
                name = "proj"
                compiler_dir = "{compiler_dir}"

                [libraries]
                dependency = {{ git = "file://{}", branch = "a-branch" }}
            "#,
                dependency.root_utf8
            )),
        );

        ((project, config), dependency)
    }

    #[test]
    fn building_with_git_dependencies_work() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir,
            spade_target,
        } = test::get_compiler();

        let ((project, config), _dep) = setup_git_dependency_project(&compiler_dir);

        let args = Command::Build.into_args();
        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        let _ = spade::build_spade(&ctx).unwrap();
    }

    #[test]
    fn lock_file_works() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir,
            spade_target,
        } = test::get_compiler();

        // Depend on a local git repository
        let ((project, config), dependency) = setup_git_dependency_project(&compiler_dir);
        let args = Command::Build.into_args();

        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        // Build
        let _ = spade::build_spade(&ctx).unwrap();

        // Create a commit in the git repository that should make it not build unless we change it.
        // Here we rename the function.
        std::fs::write(
            dependency.root.child("src").child("main.spade"),
            &unindent::unindent(
                "
                // was named 'two'
                fn three() -> int<4> {
                    3
                }
                ",
            ),
        )
        .unwrap();
        test::git::add(&dependency.root, &dependency.source_files);
        test::git::commit(&dependency.root, "rename function");

        // Run update.
        let mut lock_file = LockFile::try_open(&project.lock_file).unwrap();
        update_libraries(&project.library_dir, &ctx.config, &mut lock_file).unwrap();
        // Manually make sure the updates are written.
        lock_file.try_write().unwrap();

        // This shouldn't build since the old function was removed.
        assert!(spade::build_spade(&ctx).is_err());

        std::fs::write(
            project.root.child("src").child("main.spade"),
            &unindent::unindent(
                "
                entity top() -> bool {
                    dependency::main::three() == 3
                }
                ",
            ),
        )
        .unwrap();

        // Now it should build since we call the correct function.
        let _ = spade::build_spade(&ctx).unwrap();
    }

    #[test]
    fn re_exporting_transitive_dependencies_work() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir: _,
            spade_target,
        } = test::get_compiler();

        let (transitive_dependency, _) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                    fn two() -> int<4> {
                        2
                    }
                ",
            ),
            r#"name = "transitive_dependency""#,
        );

        let (dependency, _) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                use transitive_dependency::main::two;
                ",
            ),
            &unindent::unindent(&format!(
                r#"
                    name = "dependency"
                    [libraries]
                    transitive_dependency = {{ path = "{}" }}
                "#,
                transitive_dependency.root_utf8
            )),
        );

        let (project, config) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                    entity top() -> bool {
                        dependency::main::two() == 2
                    }
            ",
            ),
            &unindent::unindent(&format!(
                r#"
                    name = "proj"
                    [libraries]
                    dependency = {{ path = "{}" }}
                "#,
                dependency.root_utf8
            )),
        );

        let args = Command::Build.into_args();
        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        let _ = spade::build_spade(&ctx).unwrap();
    }

    mod deny_changes_to_git_dependencies {
        use camino::Utf8Path;

        use crate::{
            cmdline::Command,
            config::Config,
            libraries::{restore_libraries, LockFile},
            plugin::PluginList,
            spade,
            test::{self, Project},
            CommandCtx,
        };

        use super::setup_git_dependency_project;

        fn prepare(
            compiler_dir: &Utf8Path,
            spade_target: &Utf8Path,
        ) -> ((Project, Config), Project) {
            let ((project, config), dep) = setup_git_dependency_project(compiler_dir);

            let args = Command::Build.into_args();

            let ctx = CommandCtx {
                root_dir: &project.root_utf8.clone(),
                compiler: spade_target.into(),
                args: &args,
                config,
                plugins: PluginList::empty(),
            };
            // Build once
            spade::build_spade(&ctx).unwrap();

            // Edit the dependency in the build dir (but in a way that would still compile)
            // NOTE: This assumes the structure of the build directory.
            std::fs::write(
                &project
                    .root_utf8
                    .join("build")
                    .join("libs")
                    .join("dependency")
                    .join("src")
                    .join("main.spade"),
                &unindent::unindent(
                    "
                    fn two() -> int<5> {
                        2
                    }
                ",
                ),
            )
            .unwrap();

            ((project, ctx.config), dep)
        }

        #[test]
        fn edits_are_denied() {
            if !test::enable_spade() {
                return;
            }

            let test::SpadeDirs {
                compiler_dir,
                spade_target,
            } = test::get_compiler();

            let ((project, config), _dep) = prepare(&compiler_dir, &spade_target);

            let args = Command::Build.into_args();
            let ctx = CommandCtx {
                root_dir: &project.root_utf8,
                compiler: spade_target,
                args: &args,
                config,
                plugins: PluginList::empty(),
            };
            let _ = spade::build_spade(&ctx).map(|_| ()).unwrap_err();
        }

        #[test]
        fn restores_work() {
            if !test::enable_spade() {
                return;
            }

            let test::SpadeDirs {
                compiler_dir,
                spade_target,
            } = test::get_compiler();

            let ((project, config), _dep) = prepare(&compiler_dir, &spade_target);

            let args = Command::Build.into_args();

            let ctx = CommandCtx {
                root_dir: &project.root_utf8,
                compiler: spade_target,
                args: &args,
                config,
                plugins: PluginList::empty(),
            };
            let _ = spade::build_spade(&ctx).map(|_| ()).unwrap_err();

            // But then restore the dependencies
            let mut lock_file = LockFile::try_open(&project.lock_file).unwrap();
            restore_libraries(&project.root_utf8, &ctx.config, &mut lock_file).unwrap();

            // Should now build.
            spade::build_spade(&ctx).unwrap();
        }
    }

    #[test]
    fn empty_lockfile_sections_dont_show() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir,
            spade_target,
        } = test::get_compiler();

        let ((project, config), _dep) = setup_git_dependency_project(&compiler_dir);

        let args = Command::Build.into_args();

        let mut ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        spade::build_spade(&ctx).unwrap();

        // Test that the lock file contains a library section.
        let lock_file = LockFile::try_open(&project.lock_file).unwrap();
        assert!(lock_file.inner.libraries.is_some());

        // Remove the dependency and fix so that the code still compiles.
        std::fs::write(&project.config, r#"name = "dependency""#).unwrap();
        ctx.config = Config::read(&project.root_utf8, &None).unwrap();

        std::fs::write(
            &project.source_files[0],
            &unindent::unindent(
                "
                fn two() -> int<4> {
                    2
                }
            ",
            ),
        )
        .unwrap();

        spade::build_spade(&ctx).unwrap();

        // The library field should now have been be removed.
        let lock_file = LockFile::try_open(&project.lock_file).unwrap();
        assert!(lock_file.inner.libraries.is_none());
    }
}
