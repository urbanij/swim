use camino::Utf8Path;
use serde::Deserialize;

use color_eyre::eyre::{Context, Result};

/// Utilities for parsing cocotb results

#[derive(Deserialize, Clone)]
struct TestSuites {
    pub testsuite: Vec<TestSuite>,
}

#[derive(Deserialize, Clone)]
struct TestSuite {
    pub testcase: Vec<TestCase>,
}

#[derive(Deserialize, Clone)]
pub struct TestCase {
    pub name: String,
    pub failure: Option<Failure>,
}

#[derive(Deserialize, Clone, Debug)]
pub struct Failure {}

fn parse_results_xml(results_file: &Utf8Path) -> Result<TestSuites> {
    let file_content = std::fs::read_to_string(&results_file)
        .with_context(|| format!("Failed to read {results_file}"))?;

    quick_xml::de::from_str(&file_content)
        .with_context(|| format!("Failed to decode {results_file}"))
}

pub fn list_failed_tests(results_file: &Utf8Path) -> Result<Vec<TestCase>> {
    Ok(parse_results_xml(results_file)?
        .testsuite
        .iter()
        .flat_map(|suite| suite.testcase.iter())
        .filter_map(|case| {
            if case.failure.is_some() {
                Some((*case).clone())
            } else {
                None
            }
        })
        .collect())
}
