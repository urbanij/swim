use std::process::Command;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{anyhow, Context, Result};
use log::{debug, error, info};

use crate::{
    build_dir,
    cmdline::Args,
    compiler_dir, compiler_state_file,
    config::Config,
    libraries::{
        load_libraries, load_spade_library, update_spade_git_library, Library, LockFile,
        RestoreAction,
    },
    library_list_file, libs_dir, lock_file,
    plugin::config::BuildStep,
    preprocessing, src_dir,
    util::{make_relative, needs_rebuild},
    CommandCtx,
};

pub(crate) enum BuildTarget {
    Spade,
    VcdTranslate,
    SpadeCxx,
    #[allow(dead_code)]
    All,
}

pub fn update_spade(root_dir: &Utf8Path, config: &Config) -> Result<()> {
    match &config.compiler {
        compiler @ Library::Git(git_library) => {
            let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
            debug!("Updating compiler via git");
            update_spade_git_library(
                git_library,
                &compiler_dir(root_dir, compiler),
                &mut lock_file,
            )?;
            lock_file.try_write()?;
        }
        Library::Path(_) => info!("Compiler is set to local path in swim.toml. `update-spade` only works for compilers via git"),
    }

    Ok(())
}

pub fn get_spade_repository(
    root_dir: &Utf8Path,
    config: &Config,
    restore_action: RestoreAction,
) -> Result<()> {
    // If the compiler is a git reference, clone/checkout it if needed.
    match &config.compiler {
        Library::Git(git_library) => {
            let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
            load_spade_library(
                git_library,
                &compiler_dir(root_dir, &config.compiler),
                &mut lock_file,
                restore_action,
            )?;
            lock_file.try_write()?;
        }
        Library::Path(_) => (),
    }
    Ok(())
}

/// If the compiler is a git dependency managed by swim, restore the local repository
/// to the commit specified in swim.lock, or swim.toml if swim.lock doesn't exist.
pub fn restore_spade_repository(root_dir: &Utf8Path, config: &Config) -> Result<()> {
    get_spade_repository(root_dir, config, RestoreAction::Restore)
}

pub(crate) fn build_spade_repository(
    root_dir: &Utf8Path,
    config: &Config,
    target: BuildTarget,
    debug_spadec: bool,
    quiet: bool,
) -> Result<Utf8PathBuf> {
    let compiler_dir = compiler_dir(root_dir, &config.compiler);
    get_spade_repository(root_dir, config, RestoreAction::Deny)?;

    cargo_build(&compiler_dir, target, debug_spadec, quiet)
}

pub(crate) fn cargo_build(
    dir: &Utf8Path,
    target: BuildTarget,
    build_debug: bool,
    quiet: bool,
) -> Result<Utf8PathBuf> {
    let status = Command::new("cargo")
        .arg("build")
        .args(if build_debug {
            vec![]
        } else {
            vec!["--release"]
        })
        .args(if quiet {vec!["-q"]} else {vec![]})
        .args(match target {
            BuildTarget::Spade => vec!["--package", "spade"],
            BuildTarget::VcdTranslate => vec!["--package", "vcd-translate"],
            BuildTarget::SpadeCxx => vec!["--package", "spade-cxx"],
            BuildTarget::All => vec!["--all"],
        })
        .current_dir(dir)
        .status()
        .with_context(|| format!("Failed to run cargo build.\nEnsure that cargo is installed and a spade repository is present in {dir}"))?;

    if !status.success() {
        Err(anyhow!("Cargo exited with status {:?}", status.code()))
    } else if build_debug {
        Ok(dir.join("target/debug"))
    } else {
        Ok(dir.join("target/release"))
    }
}

#[derive(Debug)]
pub struct Namespace {
    /// Namespace in which to add items in the file to
    pub namespace: String,
    /// Name space which lib:: refers to
    pub base_namespace: String,
}

impl Namespace {
    pub fn new_lib(name: &str) -> Self {
        Self {
            namespace: name.into(),
            base_namespace: name.into(),
        }
    }
}

#[derive(Debug)]
pub struct SpadeFile {
    pub namespace: Namespace,
    pub path: Utf8PathBuf,
}

impl SpadeFile {
    fn as_compiler_arg(&self) -> String {
        let Self {
            namespace:
                Namespace {
                    namespace,
                    base_namespace,
                },
            path,
        } = self;
        format!("{base_namespace},{namespace},{}", make_relative(path))
    }
}

/// Recursively search a dir for all spade files, using the filename to infer
/// the namespace of the file.
pub fn spade_files_in_dir(
    namespace: Namespace,
    dir: impl AsRef<Utf8Path>,
) -> Result<Vec<SpadeFile>> {
    let dir = dir.as_ref();
    let paths = dir
        .read_dir_utf8()
        .with_context(|| format!("Failed to read files in {}", dir))?
        .map(|entry| entry.context("Failed to read dir entry"))
        .collect::<Result<Vec<_>>>()
        .with_context(|| format!("While reading files in {}", dir))?;

    let mut result = vec![];
    for path_utf8 in paths.iter() {
        let path = path_utf8.path();

        // If the file doesn't have a stem, it also won't have a `.spade` extension,
        // so we'll silently ignore it
        let file_stem = if let Some(stem) = path.file_stem() {
            stem
        } else {
            continue;
        };

        let new_namespace = Namespace {
            namespace: format!("{}::{}", namespace.namespace, file_stem),
            base_namespace: namespace.base_namespace.clone(),
        };
        if path.is_dir() {
            result.append(&mut spade_files_in_dir(new_namespace, path)?);
        } else if path.extension() == Some("spade") {
            result.push(SpadeFile {
                namespace: new_namespace,
                path: path.into(),
            })
        }
    }

    Ok(result)
}

/// Build the spade compiler, returning a path to the resulting binary
pub fn build_spadec(root_dir: &Utf8Path, args: &Args, config: &Config) -> Result<Utf8PathBuf> {
    info!("Building spade compiler");
    let target_dir = build_spade_repository(
        root_dir,
        config,
        BuildTarget::Spade,
        args.debug_spadec,
        args.quiet,
    )?;
    info!("Built spade compiler");
    Ok(target_dir.join("spade"))
}

/// Builds spade-cxx in release mode
pub fn build_spade_cxx(root_dir: &Utf8Path, args: &Args, config: &Config) -> Result<()> {
    info!("Building spade-cxx");
    build_spade_repository(
        root_dir,
        config,
        BuildTarget::SpadeCxx,
        args.debug_spadec,
        args.quiet,
    )?;
    info!("Built spade-cxx");
    Ok(())
}

/// Build the vcd translator, returning a path to the resulting binary
pub fn build_vcd_translate(
    root_dir: &Utf8Path,
    args: &Args,
    config: &Config,
) -> Result<Utf8PathBuf> {
    info!("Building vcd translator");
    let target_dir = build_spade_repository(
        root_dir,
        config,
        BuildTarget::VcdTranslate,
        args.debug_spadec,
        args.quiet,
    )?;
    info!("Built vcd translator");
    Ok(target_dir.join("vcd-translate"))
}

pub struct SpadecOutput {
    pub verilog: Utf8PathBuf,
    pub state_file: Utf8PathBuf,
    pub item_file: Utf8PathBuf,
}

/// Runs the spade compiler on the input source files to generate a spade file. Returns
/// the path to the generated file.
///
/// Libraries are updated as needed.
///
/// If the target is newer than the compiler and all the source files, nothing else is done.
pub fn build_spade(ctx: &CommandCtx) -> Result<SpadecOutput> {
    let CommandCtx {
        root_dir,
        args,
        config,
        compiler,
        plugins,
    } = ctx;
    let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
    debug!("Opened {:?}", lock_file);
    let library_dirs = load_libraries(
        &libs_dir(root_dir),
        config,
        &mut lock_file,
        RestoreAction::Deny,
    )?;
    if let Some(libraries) = &mut lock_file.inner.libraries {
        libraries.filter_unseen();
    }
    lock_file.try_write()?;

    std::fs::create_dir_all(build_dir(root_dir)).context("Failed to create build directory")?;
    // Write the list of libraries to a file give that information to plugins
    // NOTE: Safe unwrap, encoding of strings and paths should not fail
    std::fs::write(
        library_list_file(root_dir),
        serde_json::to_string_pretty(&library_dirs).expect("Failed to encode library dirs"),
    )
    .with_context(|| {
        format!(
            "Failed to write library list to {}",
            library_list_file(root_dir)
        )
    })?;

    preprocessing::perform_preprocessing(
        root_dir,
        config.preprocessing.as_ref().unwrap_or(&vec![]),
    )?;

    let self_files = spade_files_in_dir(Namespace::new_lib(&ctx.config.name), src_dir(root_dir))?;

    let lib_files = library_dirs
        .iter()
        .map(|(name, dir)| spade_files_in_dir(Namespace::new_lib(name), dir))
        .collect::<Result<Vec<_>>>()?
        .into_iter()
        .flatten();

    let spade_files = self_files.into_iter().chain(lib_files).collect::<Vec<_>>();

    if spade_files.is_empty() {
        return Err(anyhow!(
            "Found no source files in any of the source directories"
        ));
    }

    let state_file = compiler_state_file(root_dir);
    let item_file = build_dir(root_dir).join("items.ron");
    let spade_target = build_dir(root_dir).join("spade.sv");
    if needs_rebuild(
        &spade_target,
        spade_files
            .iter()
            .map(|f| &f.path)
            .chain([&compiler.to_path_buf()].into_iter())
            .chain([&root_dir.join("swim.toml")].into_iter())
            .chain(&plugins.rebuild_triggers(&BuildStep::SpadeBuild)),
    )? {
        info!("Building spade code");

        let mut base_command = if args.debug_spadec {
            let mut cmd = Command::new("gdb");

            cmd.arg("--args").arg(compiler);

            cmd
        } else {
            Command::new(compiler)
        };

        let status = base_command
            .args(spade_files.iter().map(|f| f.as_compiler_arg()))
            .arg("-o")
            .arg(&spade_target)
            .arg("--mir-output")
            .arg(build_dir(root_dir).join("spade.spmir"))
            .arg("--state-dump")
            .arg(&state_file)
            .arg("--item-list")
            .arg(&item_file)
            .status()
            .context("Failed to run spade compiler")?;

        if !status.success() {
            error!("Failed to build spade scode");
            Err(anyhow!("Failed to build spade code"))
        } else {
            info!("Built {}", spade_target);

            plugins.run_post_build_commands()?;
            Ok(SpadecOutput {
                verilog: spade_target,
                state_file,
                item_file,
            })
        }
    } else {
        info!("{} is up to date", spade_target);
        Ok(SpadecOutput {
            verilog: spade_target,
            state_file,
            item_file,
        })
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use crate::cmdline::Command;
    use crate::libraries::{Library, PathLibrary};
    use crate::plugin::PluginList;
    use crate::spade;
    use crate::test;
    use crate::{config, CommandCtx};

    #[test]
    fn building_only_creates_files_in_build_directory() {
        if !test::enable_spade() {
            return;
        }

        let project = test::init_repo().unwrap();

        let test::SpadeDirs {
            compiler_dir,
            spade_target,
        } = test::get_compiler();

        let args = crate::cmdline::Args::with_command(crate::cmdline::Command::Build);
        let config = crate::config::Config::new_empty(Library::Path(PathLibrary {
            path: compiler_dir.clone(),
        }));

        // Store which files we have.
        let files_before = test::files_in_dir(&project.root_utf8).unwrap();

        // Run the compiler.
        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        let _ = spade::build_spade(&ctx).unwrap();

        // Check that no other files were created.
        let files_after = test::files_in_dir(&project.root_utf8).unwrap();

        // Ignore some folders.
        let is_ignored = |path: &camino::Utf8Path| {
            [".git", "build", "swim.lock"]
                .into_iter()
                .any(|s| path.starts_with(project.root_utf8.join(camino::Utf8Path::new(s))))
        };

        let files_before: Vec<_> = files_before
            .into_iter()
            .filter(|p| !is_ignored(p))
            .collect();
        let files_after: Vec<_> = files_after.into_iter().filter(|p| !is_ignored(p)).collect();

        // Only shown if test fails.
        let files_before_set: HashSet<_> = files_before.iter().collect();
        let files_after_set: HashSet<_> = files_after.iter().collect();
        let difference = files_before_set.symmetric_difference(&files_after_set);
        println!("{difference:?}");

        assert_eq!(files_before, files_after);
    }

    #[test]
    fn building_with_spade_compiler_as_absolute_path_works() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir,
            spade_target,
        } = test::get_compiler();
        dbg!(&compiler_dir, &spade_target);

        let (project, config) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                fn yes() -> bool {
                    true
                }
            ",
            ),
            &unindent::unindent(&format!(
                r#"
                name = "proj"
                compiler = {{ path = "{compiler_dir}" }}
            "#,
            )),
        );
        dbg!(&project, &config);

        let args = Command::Build.into_args();
        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        let _ = spade::build_spade(&ctx).unwrap();
    }

    #[test]
    fn building_with_spade_compiler_as_relative_path_works() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir,
            spade_target,
        } = test::get_compiler();
        dbg!(&compiler_dir, &spade_target);

        // We need to setup the project first so we know what to take the relative path _from_.
        let (project, config) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                fn yes() -> bool {
                    true
                }
            ",
            ),
            &unindent::unindent(&format!(
                r#"
                name = "proj"
                compiler = {{ path = "{compiler_dir}" }}
            "#,
            )),
        );
        dbg!(&project, &config);

        // Overwrite the config with a new config containing the relative path.
        let compiler_rel_dir = pathdiff::diff_utf8_paths(compiler_dir, &project.root_utf8).unwrap();
        dbg!(&compiler_rel_dir);
        let config_rel_dir_content = format!(
            r#"
            name = "proj"
            compiler = {{ path = "{compiler_rel_dir}" }}
        "#
        );
        std::fs::write(project.config, config_rel_dir_content).unwrap();
        let config = config::Config::read(&project.root_utf8, &None).unwrap();

        let args = Command::Build.into_args();
        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        let _ = spade::build_spade(&ctx).unwrap();
    }

    #[test]
    fn building_with_spade_compiler_as_git_works() {
        if !test::enable_spade() {
            return;
        }

        let test::SpadeDirs {
            compiler_dir,
            spade_target,
        } = test::get_compiler();
        dbg!(&compiler_dir, &spade_target);

        let (project, config) = test::init_spade_repo_with(
            &unindent::unindent(
                "
                fn yes() -> bool {
                    true
                }
            ",
            ),
            &unindent::unindent(&format!(
                r#"
                name = "proj"
                compiler = {{ git = "file://{compiler_dir}", branch = "master" }}
            "#,
            )),
        );
        dbg!(&project, &config);

        let args = Command::Build.into_args();
        let ctx = CommandCtx {
            root_dir: &project.root_utf8,
            compiler: spade_target,
            args: &args,
            config,
            plugins: PluginList::empty(),
        };
        let _ = spade::build_spade(&ctx).unwrap();
    }
}
