use std::env::current_dir;
use std::io;
use std::process;
use std::time::SystemTime;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::{
    eyre::{anyhow, Context},
    Result, Section,
};
use local_impl::local_impl;
use log::{debug, error};
use pathdiff::diff_utf8_paths;

fn log_output(stdout: &[u8], stderr: &[u8]) {
    let stdout = String::from_utf8_lossy(stdout);
    let stderr = String::from_utf8_lossy(stderr);

    if !stdout.is_empty() {
        debug!("[stdout]:\n{}", stdout.as_ref());
    }
    if !stderr.is_empty() {
        debug!("[stderr]:\n{}", stderr.as_ref());
    }
}

#[local_impl]
impl CommandExt for process::Command {
    /// Output a debug log of the command and args.
    fn log_command(&mut self) -> &mut Self {
        debug!("{self:?}\nworking dir: {:?}", self.get_current_dir());
        self
    }

    /// Run the command and return the status. Output is put in the log.
    fn status_and_log_output(&mut self) -> io::Result<process::ExitStatus> {
        let process::Output {
            status,
            stdout,
            stderr,
        } = self.output()?;
        log_output(&stdout, &stderr);
        Ok(status)
    }

    /// Run the command and return the status. Output is put in the log if we're running tests,
    /// otherwise to normal stdout/stderr.
    fn status_and_log_output_if_tests(&mut self) -> io::Result<process::ExitStatus> {
        if cfg!(test) {
            self.status_and_log_output()
        } else {
            self.status()
        }
    }

    /// Run the command and return the status. Output is put in the log if the command fails or
    /// we're running tests.
    fn status_and_log_output_if_tests_or_errs(&mut self) -> io::Result<process::ExitStatus> {
        let process::Output {
            stdout,
            stderr,
            status,
        } = self.output()?;
        if !status.success() {
            log_output(&stdout, &stderr);
        }
        Ok(status)
    }

    /// Run the command and return the output. Output is also put into the log.
    fn output_and_log_output(&mut self) -> io::Result<process::Output> {
        let output = self.output()?;
        log_output(&output.stdout, &output.stderr);
        Ok(output)
    }

    /// Run the command, returning any error encountered when running the command (e.g. command not
    /// found), or the given error if the command exited with an exit code indicating failure.
    fn success_or_else<F, U>(&mut self, err: F) -> std::result::Result<(), U>
    where
        F: FnOnce() -> U,
        U: From<io::Error>,
    {
        // NOTE: This is the impl on [ExitStatus]
        self.status()?.success_or_else(err)
    }
}

#[local_impl]
impl ExitStatusExt for process::ExitStatus {
    fn success_or_else<F, U>(&mut self, err: F) -> std::result::Result<(), U>
    where
        F: FnOnce() -> U,
    {
        self.success().then(|| ()).ok_or_else(err)
    }
}

#[local_impl]
impl OutputExt for process::Output {
    fn stdout_str(&self) -> std::borrow::Cow<'_, str> {
        String::from_utf8_lossy(&self.stdout)
    }
}

/// Aligns to strings to the left and to the right according to some widths, respectively.
#[derive(Debug)]
pub struct LineAligner {
    pub left: String,
    pub right: String,
    pub width_left: usize,
    pub width_right: usize,
}

impl std::fmt::Display for LineAligner {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Self {
            left,
            right,
            width_left,
            width_right,
        } = self;
        write!(f, "{left:<width_left$} {right:>width_right$}")
    }
}

/// Returns all files present in the specified directory
pub fn recursive_files_in_dir(dir: &Utf8Path, extension: &str) -> Result<Vec<Utf8PathBuf>> {
    let paths = dir
        .read_dir_utf8()
        .with_context(|| format!("Failed to read files in {}", dir))?
        .map(|entry| entry.context("Failed to read dir entry"))
        .collect::<Result<Vec<_>>>()
        .with_context(|| format!("While reading files in {}", dir))?;

    let paths = paths.iter().map(|entry| entry.path()).collect::<Vec<_>>();

    let mut files = paths
        .iter()
        .filter(|path| path.is_file() && path.extension() == Some(extension))
        .map(|path| path.to_path_buf())
        .collect::<Vec<_>>();

    files.append(
        &mut paths
            .iter()
            .filter(|path| path.is_dir())
            .map(|dir| recursive_files_in_dir(dir, extension))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .flatten()
            .collect(),
    );

    Ok(files)
}

#[derive(Debug, thiserror::Error)]
#[error("{0}")]
pub struct StringError(String);

/// Recursively copies all files and directories in the directory `from` to the directory `to`.
///
/// Will try to copy as much as possible, even if an error occurs. No copied files are removed on
/// error.
pub fn copy_recurse<P1: AsRef<Utf8Path>, P2: AsRef<Utf8Path>>(from: P1, to: P2) -> Result<()> {
    // iterate all entries
    let results: Vec<_> = from
        .as_ref()
        .read_dir_utf8()?
        .map(|entry| match &entry {
            Ok(entry) if entry.path().is_dir() => {
                let copy_target = to.as_ref().join(
                    entry
                        .path()
                        .file_name()
                        .ok_or_else(|| anyhow!("Directory without name: {entry:?}"))?,
                );
                std::fs::create_dir(&copy_target)
                    .map_err(|e| anyhow!("When creating dir {:?}: {e}", copy_target))?;
                copy_recurse(entry.path(), copy_target)
            }
            Ok(entry) if entry.path().is_file() => {
                let copy_target = to.as_ref().join(
                    entry
                        .path()
                        .file_name()
                        .ok_or_else(|| anyhow!("File without name: {entry:?}"))?,
                );
                std::fs::copy(entry.path(), copy_target)?;
                Ok(())
            }
            Ok(entry) => Err(anyhow!("{entry:?} does not exist")),
            e => Err(anyhow!("{e:?} ({entry:?})")),
        })
        .collect();

    // https://github.com/yaahc/color-eyre/blob/2128f074a9ae747612257f5ac02438d2a8068471/examples/multiple_errors.rs
    if results.iter().all(|r| r.is_ok()) {
        Ok(())
    } else {
        results
            .into_iter()
            .filter(Result::is_err)
            .map(Result::unwrap_err)
            .fold(Err(anyhow!("Errors occured:")), |report, e| {
                // FIXME: The StringError is ugly here. Find a way to remove it.
                report.error(StringError(format!("{e}")))
            })
    }
}

pub fn modified_time(path: &Utf8Path) -> Result<SystemTime> {
    std::fs::metadata(path)
        .with_context(|| format!("Failed to get metadata of {:?}", path))?
        .modified()
        .with_context(|| format!("Failed to get modified time of {:?}", path))
}

pub fn needs_rebuild<'a>(
    target: &Utf8Path,
    sources: impl IntoIterator<Item = &'a Utf8PathBuf>,
) -> Result<bool> {
    if !target.exists() {
        Ok(true)
    } else {
        let target_time = modified_time(target)?;

        Ok(sources
            .into_iter()
            .map(|source| modified_time(source))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .any(|source_time| source_time > target_time))
    }
}

pub fn pluralize(n: usize) -> &'static str {
    if n == 1 {
        ""
    } else {
        "s"
    }
}

/// Makes the specified path relative to the working directory. If it can not be made relative
/// it will return the original path
pub fn make_relative(path: &impl AsRef<Utf8Path>) -> Utf8PathBuf {
    let curr = Utf8PathBuf::try_from(current_dir().unwrap()).unwrap();
    diff_utf8_paths(&path, curr).unwrap_or(path.as_ref().to_owned())
}

/// Makes the specified path relative to the working directory. If it can not be made relative
/// it will return the original path
pub fn make_relative_to(path: &impl AsRef<Utf8Path>, to: &impl AsRef<Utf8Path>) -> Utf8PathBuf {
    diff_utf8_paths(path, to).unwrap_or(path.as_ref().to_owned())
}

#[cfg(test)]
mod tests {
    use std::thread;
    use std::time::Duration;

    use crate::util::needs_rebuild;

    use assert_fs::prelude::*;
    use camino::Utf8PathBuf;

    const FS_DELAY: Duration = Duration::from_millis(10);

    #[test]
    fn needs_rebuild_works() {
        let dir = assert_fs::TempDir::new().unwrap();
        let source = dir.child("source");
        let target = dir.child("target");
        let source_utf8: Utf8PathBuf = source.to_path_buf().try_into().unwrap();
        let target_utf8: Utf8PathBuf = target.to_path_buf().try_into().unwrap();
        source.touch().unwrap();
        target.touch().unwrap();

        // Target should already be up to date
        assert!(!needs_rebuild(&target_utf8, [&source_utf8]).unwrap());

        // If we don't wait the files still have the same modified date and the test doesn't work.
        thread::sleep(FS_DELAY);

        // Writing to source should warrant a rebuild.
        source.touch().unwrap();
        assert!(needs_rebuild(&target_utf8, [&source_utf8]).unwrap());

        // Simulate building target again.
        thread::sleep(FS_DELAY);
        target.touch().unwrap();
        assert!(!needs_rebuild(&target_utf8, [&source_utf8]).unwrap());
    }

    #[test]
    fn needs_rebuild_with_multiple_sources_works() {
        let dir = assert_fs::TempDir::new().unwrap();
        let source1 = dir.child("source1");
        let source2 = dir.child("source2");
        let target = dir.child("target");
        let source1_utf8: Utf8PathBuf = source1.to_path_buf().try_into().unwrap();
        let source2_utf8: Utf8PathBuf = source2.to_path_buf().try_into().unwrap();
        let target_utf8: Utf8PathBuf = target.to_path_buf().try_into().unwrap();
        source1.touch().unwrap();
        source2.touch().unwrap();
        target.touch().unwrap();

        // Target should already be up to date
        assert!(!needs_rebuild(&target_utf8, [&source1_utf8, &source2_utf8]).unwrap());

        // If we don't wait the files still have the same modified date and the test doesn't work.
        thread::sleep(FS_DELAY);

        // Writing to any source should warrant a rebuild.
        source1.touch().unwrap();
        assert!(needs_rebuild(&target_utf8, [&source1_utf8, &source2_utf8]).unwrap());

        thread::sleep(FS_DELAY);
        target.touch().unwrap();
        assert!(!needs_rebuild(&target_utf8, [&source1_utf8, &source2_utf8]).unwrap());

        // Write to the other one too.
        thread::sleep(FS_DELAY);
        source2.touch().unwrap();
        assert!(needs_rebuild(&target_utf8, [&source1_utf8, &source2_utf8]).unwrap());
    }
}
