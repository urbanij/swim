use color_eyre::eyre::Result;
use std::{process::Stdio, sync::Arc};

use async_trait::async_trait;
use tokio::{
    io::AsyncReadExt,
    process::{ChildStderr, ChildStdout},
    sync::Mutex,
};

#[async_trait]
pub trait StdioHandler {
    fn cmd_out(&self) -> Stdio;
    fn cmd_err(&self) -> Stdio;

    // Start processing the stdio
    async fn process(
        &mut self,
        stdout: &mut Option<ChildStdout>,
        stderr: &mut Option<ChildStderr>,
    ) -> Result<()>;
}

pub struct StdioInherit;

#[async_trait]
impl StdioHandler for StdioInherit {
    fn cmd_out(&self) -> Stdio {
        Stdio::inherit()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::inherit()
    }

    async fn process(
        &mut self,
        _stdout: &mut Option<ChildStdout>,
        _stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        Ok(())
    }
}

pub struct StdioAccumulator {
    stdio: Arc<Mutex<String>>,
}

impl StdioAccumulator {
    pub fn new(stdio: Arc<Mutex<String>>) -> Self {
        Self { stdio }
    }
}

#[async_trait]
impl StdioHandler for StdioAccumulator {
    fn cmd_out(&self) -> Stdio {
        Stdio::piped()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::piped()
    }

    async fn process(
        &mut self,
        stdout: &mut Option<ChildStdout>,
        stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        let stdout = stdout.as_mut().unwrap();
        let stderr = stderr.as_mut().unwrap();
        tokio::select! {
            new = read_to_vec(stdout) => {
                *self.stdio.lock().await += &String::from_utf8_lossy(&new?);
            }
            new = read_to_vec(stderr) => {
                *self.stdio.lock().await += &String::from_utf8_lossy(&new?);
            }
        };
        Ok(())
    }
}

// Reads stdout into the contained mutex, prints stderr
pub struct StdoutCollector {
    stdout: Arc<Mutex<String>>,
}

impl StdoutCollector {
    pub fn new(stdout: Arc<Mutex<String>>) -> Self {
        Self { stdout }
    }
}

#[async_trait]
impl StdioHandler for StdoutCollector {
    fn cmd_out(&self) -> Stdio {
        Stdio::piped()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::inherit()
    }

    async fn process(
        &mut self,
        stdout: &mut Option<ChildStdout>,
        _stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        let stdout = stdout.as_mut().unwrap();
        tokio::select! {
            new = read_to_vec(stdout) => {
                *self.stdout.lock().await += &String::from_utf8_lossy(&new?);
            }
        };
        Ok(())
    }
}

async fn read_to_vec(stream: &mut (impl AsyncReadExt + Unpin)) -> Result<Vec<u8>> {
    let mut buf = vec![];
    stream.read_buf(&mut buf).await?;
    Ok(buf)
}
