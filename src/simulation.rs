use std::{collections::HashMap, sync::Arc};

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{anyhow, bail, Context, Result};
use indoc::formatdoc;
use itertools::Itertools;
use log::{info, warn};
use serde::Deserialize;
use tokio::{process::Command, sync::Mutex};
use tokio_stream::StreamExt;

use crate::{
    build_dir,
    cmdline::{Args, SimulationArgs},
    cocotb::{list_failed_tests, Failure},
    compiler_dir,
    config::{Config, Simulation},
    par_sim_ui::{run_parallel_commands, CommandStatus, UiConfig},
    spade::build_spade,
    spade::SpadecOutput,
    stdio_handling::{StdioAccumulator, StdioHandler, StdioInherit, StdoutCollector},
    util::{make_relative, make_relative_to, needs_rebuild, recursive_files_in_dir},
    venv_path, CommandCtx,
};

/// Same format as `spade_compiler::name_dump`
#[derive(Deserialize, Clone)]
pub enum ItemKind {
    /// The item is a unit which is not generic and can thus easily be
    /// referred to
    Normal(String),
    /// The item exists, is a unit but is generic and there is therefore
    /// not an easy mapping between the path and name
    Generic,
    /// The item is a type
    Type,
}

/// The result of executing all the tests in a test bench file
#[derive(Clone, Debug)]
pub struct TestFileResult {
    // Path to the test relative to the test bench directory for human readable
    // prints
    pub top_path: Vec<String>,
    pub relative_tb: Utf8PathBuf,
    pub test: Test,
    pub vcd_file: Utf8PathBuf,
    pub failure: Option<Failure>,
}

pub struct SimulationResult {
    pub result: Vec<TestFileResult>,
}

#[derive(Debug, Clone)]
pub struct Test {
    pub file: Utf8PathBuf,
    pub test_name: String,
}

impl Test {
    // The directory in which to store the output if this test. Relative to the
    // build dir
    fn test_dir(&self, simulation_config: &Simulation) -> Utf8PathBuf {
        make_relative_to(
            &self.file.parent().unwrap(),
            &simulation_config.testbench_dir,
        )
        .join(format!(
            "{}_{}",
            self.file.file_stem().unwrap(),
            self.test_name
        ))
    }
}

/// Initialises a venv and installs spade-python to it. Requires python and
/// pip to be available
async fn setup_venv(root_dir: &Utf8Path, config: &Config) -> Result<()> {
    let venv_done_marker = build_dir(root_dir).join(".venv_done");

    if needs_rebuild(&venv_done_marker, &["swim.toml".into()])? {
        let result = Command::new("python3")
            .arg("-m")
            .arg("venv")
            .arg(&venv_path(root_dir))
            .status()
            .await?;

        if !result.success() {
            return Err(anyhow!("Failed to create python virtual envronment"));
        }

        let pip_command = format!(
            "pip3 install maturin {}",
            config
                .simulation_config()?
                .python_deps
                .iter()
                .flatten()
                .join(" ")
        );

        let maturin_install_result =
            do_in_venv(root_dir, &pip_command, &[], &mut StdioInherit).await?;

        if !maturin_install_result.success() {
            bail!("Failed to install maturin");
        }

        std::fs::write(&venv_done_marker, "")
            .with_context(|| format!("Failed to write {venv_done_marker}"))?;
    }

    // NOTE: Maturin seems to rebuild the python wheel stuff even if no changes occur. This is annoying so previously we
    // manually did rebuild checking here by calling cargo build, and if the produced artefact is
    // newer than our marker, we rebuild the lib in the venv
    // However, that took longer than the maturin develop overhead, so it is disabled for now
    // lifeguard https://github.com/PyO3/maturin/issues/884
    // lifeguard https://github.com/PyO3/pyo3/issues/1708
    // if needs_rebuild("build/maturin_rebuild_tag")

    let compiler_dir = compiler_dir(root_dir, &config.compiler);

    let maturin_result = do_in_venv(
        root_dir,
        &format!("cd {compiler_dir}/spade-python && maturin develop"),
        &[],
        &mut StdioInherit,
    )
    .await
    .context("Failed to run maturin in venv")?;

    if !maturin_result.success() {
        bail!("Failed to build spade compiler python plugin")
    }

    Ok(())
}

/// Runs the specified command string in a subshell in the specified venv
pub async fn do_in_venv(
    root_dir: &Utf8Path,
    cmd_str: &str,
    extra_env: &[(String, String)],
    stdio_handler: &mut impl StdioHandler,
) -> Result<std::process::ExitStatus> {
    let activate_path = venv_path(root_dir).join("bin/activate");

    let full = format!(". {activate_path} && {cmd_str}");

    let mut cmd = Command::new("sh")
        .arg("-c")
        .arg(full)
        .envs(extra_env.iter().cloned())
        .stdout(stdio_handler.cmd_out())
        .stderr(stdio_handler.cmd_err())
        .spawn()?;

    let mut stdout = cmd.stdout.take();
    let mut stderr = cmd.stderr.take();

    let mut status = Box::pin(cmd.wait());

    loop {
        tokio::select! {
            result = &mut status => {
                break Ok(result?)
            }
            _ = stdio_handler.process(&mut stdout, &mut stderr) => {}
        }
    }
}

fn get_uut_name(tb: &Utf8Path) -> Result<String> {
    let file_content =
        std::fs::read_to_string(tb).with_context(|| format!("Failed to read {tb}"))?;

    let first_line = file_content
        .lines()
        .filter(|s| !s.is_empty())
        .next()
        .ok_or_else(|| anyhow!("{tb} is empty"))?
        .to_string();

    match first_line.chars().next() {
        Some('#') => {}
        Some(_) => bail!(
            "The first line of a test bench must be a comment specifying the module under test \n
            # top=path::to::uut

            {tb} started with {first_line}"
        ),
        None => unreachable!("We already made sure there is at least one non-empty line"),
    };

    // NOTE: This index operation is safe because we know the first byte is #, so
    // we can safely index
    let rest_line = &first_line
        .get(1..)
        .expect("Expected at least one line in tb");

    let split = rest_line.split("=").map(String::from).collect::<Vec<_>>();
    let (lhs, rhs) = match split.as_slice() {
        [lhs, rhs] => (lhs, rhs),
        _ => bail!("Expected {tb} to start with # top=path::to::uut. Got {first_line}"),
    };

    if lhs.trim() != "top" {
        bail!("{tb}:1: The first line of a test bench must be `top=path::to::uut`");
    }

    let top = rhs.trim();

    Ok(top.to_string())
}

/// Runs a cocotb test suite at the specified path. Returns an error if any
/// underlying command returns exit 1, and a list of failed test cases if test
/// running is successful, along with the vcd file to which the waves were dumped
pub async fn run_cocotb(
    root_dir: &Utf8Path,
    test: &Test,
    top_verilog_name: &str,
    top_spade_name: &str,
    spade_verilog: &Utf8Path,
    spade_state: &Utf8Path,
    spade_path: String,
    stdio_handler: &mut impl StdioHandler,
    config: &Config,
    simulation_config: &Simulation,
) -> Result<(Option<Failure>, Utf8PathBuf)> {
    let global_extra_verilog = config.extra_verilog.clone().unwrap_or(vec![]);
    let global_extra_verilog_str = global_extra_verilog
        .iter()
        .map(|v| root_dir.join(format!("{v}")))
        .join(" ");

    let spade_src = root_dir.join(spade_verilog);

    let tb_stem = test.file.file_stem().unwrap();

    // cocotb is annoying in at least 2 ways: it creates temproary files like __pycache__, vcd
    // files etc next to the python files, and it fails to re-compile the same file with different
    // configurations. To solve both those issues, we'll create a directory in our build directory,
    // copy the tb file in there and run the tests that way

    let sim_dir = build_dir(root_dir).join(test.test_dir(simulation_config));
    std::fs::create_dir_all(&sim_dir)
        .with_context(|| format!("Failed to create dir {}", sim_dir))?;
    let actual_tb_path = sim_dir.join(test.file.file_name().unwrap());
    std::fs::copy(&test.file, &actual_tb_path)
        .with_context(|| format!("Failed to copy {} into {actual_tb_path}", test.file))?;

    let test_name = &test.test_name;

    let vcd_filename = sim_dir
        .join(test.file.file_name().unwrap())
        .with_extension("vcd");
    let wal_meta_filename = sim_dir
        .join(test.file.file_name().unwrap())
        .with_extension("wal");

    let simulator = &config.simulation_config()?.simulator;
    let (extra_args, extra_plusargs, sim_args) = match simulator.as_str() {
        // FIXME -Wno-fatal is needed because we have a few warnings in the emitted code which
        // verilog catches
        // SYNTHESIS=1 ensures that we don't compile constructs not supported by verilator,
        // in particular: assertions which currently use #0, and the wave dump commands
        // used by other simulators
        "verilator" => ("-Wno-fatal --trace --trace-structs", "+SYNTHESIS=1", ""),
        // -n tells vvp to exit on ctrl-c, rather than going into an interactive shell
        // which is inaccessible to us anyway.
        "icarus" => ("", "", "-n"),
        _ => ("", "", ""),
    };

    let make_cmd = format!(
        r#"
    make \
        -C {sim_dir} \
        -f $(cocotb-config --makefiles)/Makefile.sim \
        VERILOG_SOURCES='{spade_src} {global_extra_verilog_str}' \
        TOPLEVEL='{top_verilog_name}' \
        MODULE={tb_stem} \
        TESTCASE={test_name} \
        SIM={simulator} \
        EXTRA_ARGS="{extra_args}" \
        SIM_ARGS={sim_args} \
        -s \
        PLUSARGS="+TOP_MODULE={top_verilog_name} +VCD_FILENAME={vcd_filename} {extra_plusargs}"
    "#
    );

    let envs = [
        (
            "SWIM_SPADE_STATE".to_string(),
            root_dir.join(spade_state).to_string(),
        ),
        ("SWIM_UUT".to_string(), spade_path),
        ("SWIM_ROOT".to_string(), root_dir.to_string()),
    ];

    do_in_venv(root_dir, &make_cmd, &envs, stdio_handler).await?;

    let result = list_failed_tests(&sim_dir.join("results.xml"))
        .map(|list| (list, sim_dir.join(vcd_filename)));

    std::fs::write(
        wal_meta_filename,
        format!(r#"(define top_name "{top_spade_name}")"#),
    )?;

    let result = result?;
    if result.0.len() > 1 {
        bail!("Got more than one failed test from cocotb.")
    } else {
        Ok((result.0.first().map(|_| Failure {}), result.1.clone()))
    }
}

/// Discover all the tests we can run in the specified testbench
/// This is a bit of a hack, we call out to python, running an internal cocotb method
/// to discover tests. Hopefully cocotb does not remove that method in the future
async fn discover_tests(root_dir: &Utf8Path, tb: &Utf8Path) -> Result<Vec<String>> {
    let test_disovery = formatdoc!(
        r#"
        import cocotb.regression as r;
        tests = r.RegressionManager._discover_tests;
        print("\n".join(list(map(lambda t: t.name, r.RegressionManager._discover_tests()))))"#
    );

    let testbench_dir = tb.parent().unwrap();
    let test_python = tb.file_name().expect("Found no .py extension");

    let cmd = format!("cd {testbench_dir} && python3 -c '{test_disovery}' {test_python}");

    let cmd_out = Arc::new(Mutex::new(String::new()));
    let status = do_in_venv(
        root_dir,
        &cmd,
        &[
            ("MODULE".to_string(), tb.file_stem().unwrap().to_string()),
            // We don't want to pollute the testbench dir with a pycache
            ("PYTHONDONTWRITEBYTECODE".to_string(), "1".to_string()),
            ("SWIM_ROOT".to_string(), root_dir.to_string()),
        ],
        &mut StdoutCollector::new(cmd_out.clone()),
    )
    .await?;

    if !status.success() {
        bail!("Failed to discover tests in {tb}")
    }

    let result = cmd_out
        .lock()
        .await
        .lines()
        .map(|s| s.to_string())
        .collect::<Vec<_>>();

    info!("Discovered {} tests", result.len());

    Ok(result)
}

/// Simulates the source files throwing an error if compilation fails, or
/// if the tests fail.
///
/// Returns a list of vcd files which should have been generated. However, as it
/// is up to the test bench to actually generate the test files, they may not
/// actually exist after simulation
pub async fn simulate(
    ctx: &CommandCtx<'_>,
    args: &Args,
    sim_args: &SimulationArgs,
) -> Result<SimulationResult> {
    let CommandCtx {
        root_dir,
        args: _,
        config,
        compiler: _,
        plugins: _,
    } = ctx;

    let SpadecOutput {
        verilog: spade_target,
        state_file,
        item_file,
    } = build_spade(ctx)?;

    let item_list_str = std::fs::read_to_string(&item_file)
        .with_context(|| format!("Failed to read item list at {item_file}"))?;
    let item_list = ron::from_str::<HashMap<Vec<String>, ItemKind>>(&item_list_str)
        .with_context(|| format!("Failed to decode {item_file} as ron"))?;

    setup_venv(root_dir, config).await?;

    let simulation_config = config.simulation_config()?;

    let tb_files = recursive_files_in_dir(&simulation_config.testbench_dir, "py")?;

    let tests_to_run = tokio_stream::iter(tb_files)
        .filter(|tb| {
            if let Some(filter) = &sim_args.testbench_filter {
                tb.as_str().contains(filter)
            } else {
                true
            }
        })
        .then(|tb| async move { Ok((tb.clone(), discover_tests(&root_dir, &tb).await?)) })
        .collect::<Result<Vec<_>>>()
        .await?
        .into_iter()
        .flat_map(|(tb, tests)| {
            tests.into_iter().map(move |t| Test {
                file: tb.clone(),
                test_name: t.clone(),
            })
        })
        .collect::<Vec<_>>();

    let result = run_parallel_commands(
        args.num_threads(),
        tests_to_run,
        |test, stdio, result: Arc<Mutex<Vec<_>>>| {
            let result = result.clone();
            let state_file = state_file.clone();
            let spade_target = spade_target.clone();
            let item_list = item_list.clone();
            async move {
                let top_name = get_uut_name(&test.file)?;

                let mut top_parts = top_name
                    .split("::")
                    .map(|ident| ident.to_string())
                    .collect::<Vec<_>>();

                // We need to insert proj:: for namespacing to work correctly
                top_parts.insert(0, config.name.clone());

                let top_verilog_name = item_list
                    .get(&top_parts)
                    .ok_or_else(|| {
                        // A common pitfall here is most likely to try `a` when you in fact
                        // mean `main::a`. Try to be helpful if that is the case
                        top_parts.insert(1, "main".to_string());

                        if item_list.get(&top_parts).is_some() {
                            anyhow!(
                                "Failed to find module {top_name}. Did you mean main::{top_name}?"
                            )
                        } else {
                            anyhow!("No item named {top_name}")
                        }
                    })
                    .and_then(|kind| match kind {
                        ItemKind::Normal(name) => Ok(name),
                        ItemKind::Generic => Err(anyhow!(
                            "{top_name} is generic which is currently unsupported in test benches"
                        )),
                        ItemKind::Type => Err(anyhow!("{top_name} is a type")),
                    })
                    .context("In {tb}")?;

                let (failure, vcd_file) = run_cocotb(
                    root_dir,
                    &test,
                    &format!("{top_verilog_name}"),
                    &top_parts.join("::"),
                    &spade_target,
                    &state_file,
                    top_parts.join("::"),
                    &mut StdioAccumulator::new(stdio),
                    config,
                    simulation_config,
                )
                .await?;

                let relative_tb = simulation_config
                    .testbench_dir_original
                    .join(test.file.file_name().unwrap());

                result.lock().await.push(TestFileResult {
                    top_path: top_parts,
                    relative_tb,
                    test,
                    vcd_file,
                    failure: failure.clone(),
                });

                match failure {
                    Some(_) => Ok(CommandStatus::Failure),
                    None => Ok(CommandStatus::Success),
                }
            }
        },
        |t| format!("{} [{}]", make_relative(&t.file), t.test_name),
        UiConfig::test(),
    )
    .await?;

    let result = result.iter().map(|x| x.clone()).collect::<Vec<_>>();

    let result = Ok(SimulationResult { result });
    result
}

/// Translates variables in the provided VCD files according to the types defined
/// in type_file. A VCD file which does
/// not exist prints a warning but does not return Err since the test bench might
/// not correctly generate the VCD file
/// Returns a map of vcd file to translated vcd file
pub async fn translate_vcds(
    args: &Args,
    vcd_translator: &Utf8Path,
    vcd_files: Vec<(Vec<String>, Utf8PathBuf)>,
    state_file: Utf8PathBuf,
) -> Result<HashMap<Utf8PathBuf, Option<Utf8PathBuf>>> {
    let result = run_parallel_commands(
        args.num_threads(),
        vcd_files,
        |(top_path, file): (Vec<String>, Utf8PathBuf),
         _stdio,
         result: Arc<Mutex<HashMap<_, _>>>| {
            let state_file = state_file.clone();

            async move {
                let vcd_file = file;
                if !vcd_file.exists() {
                    warn!("Could not translate types in {vcd_file:?} because it does not exist");
                    result.lock().await.insert(vcd_file.clone(), None);
                    return Ok(CommandStatus::Failure);
                }

                let target_file = vcd_file.with_extension("t.vcd");
                let status =
                    if needs_rebuild(&target_file, vec![&vcd_translator.to_path_buf(), &vcd_file])?
                    {
                        // NOTE: We just inherit stdout here, because VCDtranslator
                        // is unlikely to print anythign interesting
                        let status = Command::new(&vcd_translator)
                            .arg(&vcd_file)
                            .arg("-s")
                            .arg(&state_file)
                            .arg("-o")
                            .arg(&target_file)
                            .arg("-t")
                            .arg(&top_path.join("::"))
                            .status()
                            .await
                            .with_context(|| "Failed to run vcd_translate")?;

                        if !status.success() {
                            Ok(CommandStatus::Failure)
                        } else {
                            Ok(CommandStatus::Success)
                        }
                    } else {
                        Ok(CommandStatus::Success)
                    };

                result
                    .lock()
                    .await
                    .insert(vcd_file.clone(), Some(target_file));

                status
            }
        },
        |(_, file)| file.to_string(),
        UiConfig::command("Translating VCDs"),
    )
    .await;

    result
}
